package br.com.olaescola.uirapuru.fragment;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.FileNotFoundException;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.activity.ChatMessagesActivity;

/**
 * Created by rafae on 20/06/2017.
 */

public class ImageConfirmationFragment extends DialogFragment {

    private Bitmap bitmap;
    private ImageView imageView;
    private EditText edtSubtitle;
    private FloatingActionButton btnSendMessage;

    public ImageConfirmationFragment(){
    }

    public static ImageConfirmationFragment newInstance(Bitmap bitmap) {
        ImageConfirmationFragment frag = new ImageConfirmationFragment();
        frag.bitmap = bitmap;
        return frag;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.image_preview, container);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        imageView = (ImageView) view.findViewById(R.id.imagePreview);
        imageView.setImageBitmap(bitmap);

        btnSendMessage = (FloatingActionButton) view.findViewById(R.id.btnSendMessage);
        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ((ChatMessagesActivity)getActivity()).sendImage(edtSubtitle.getText().toString());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        edtSubtitle = (EditText) view.findViewById(R.id.edtSubtitle);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
        view.setLayoutParams(layoutParams);
    }

    @Override
    public void onDestroyView() {
        Dialog dialog = getDialog();
        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public String getSubtitle(){
        return edtSubtitle.getText().toString();
    }

}
