package br.com.olaescola.uirapuru.helper;

import android.os.Environment;
import android.util.Log;

import com.mikepenz.iconics.utils.Utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * Created by developer on 22/06/17.
 */

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {
   final Thread.UncaughtExceptionHandler uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
   @Override
   public void uncaughtException(Thread thread, Throwable throwable) {
      if (uncaughtExceptionHandler != null) {
         System.out.println(throwable.getStackTrace()[0].getLineNumber());
         uncaughtExceptionHandler.uncaughtException(thread, throwable);
      } else {
         // kill process
         System.exit(-1);
      }
   }
}
