package br.com.olaescola.uirapuru.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.helper.BackgroundHelper;
import br.com.olaescola.uirapuru.helper.ChatHelper;
import br.com.olaescola.uirapuru.helper.DateHelper;
import br.com.olaescola.uirapuru.helper.FontManager;
import br.com.olaescola.uirapuru.helper.ImageHelper;
import br.com.olaescola.uirapuru.model.Chat;

import static br.com.olaescola.uirapuru.R.drawable;
import static br.com.olaescola.uirapuru.R.id;
import static br.com.olaescola.uirapuru.R.layout;
import static br.com.olaescola.uirapuru.R.string;

/**
 * Created by rafae on 18/06/2017.
 */

public class ChatMessageAdapter extends RecyclerView.Adapter {

    private final String chatId;
    private final ChatHelper chatHelper;
    private final List<Chat.Message> selectedMessages;
    private final List<Chat.Message> messages;
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private final Context context;
    private String colorStatus;

    public ChatMessageAdapter(List messages, List selectedMessages, Context context, ChatHelper chatHelper) {
        
        this.messages = messages;
        this.context = context;
        this.chatId = chatHelper.getContactId();
        this.chatHelper = chatHelper;
        this.selectedMessages = selectedMessages;
    }

    public List<Chat.Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Chat.Message> newMessages) {
        for(Chat.Message newMessage: newMessages) {
            boolean updated = false;
            int index = 0;
            for (Chat.Message oldMessage : this.messages) {
                if (oldMessage.equals(newMessage)) {
                    this.messages.set(index, newMessage);
                    updated = true;
                    break;
                } else index++;
            }
            if (!updated) this.messages.add(newMessage);
        }

        List<Chat.Message> removedMessages = new ArrayList<>();
        for(Chat.Message message: messages){
            if(message.isDeleted()) removedMessages.add(message);
        }
        messages.removeAll(removedMessages);
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case 0x00:
                view = LayoutInflater.from(context)
                        .inflate(layout.message_right, parent, false);
                break;
            case 0x10:
                view = LayoutInflater.from(context)
                        .inflate(layout.message_right_image, parent, false);
                break;
            case 0x20:
                break;
            case 0x01:
                view = LayoutInflater.from(context)
                        .inflate(layout.message_left, parent, false);
                break;
            case 0x11:
                view = LayoutInflater.from(context)
                        .inflate(layout.message_left_image, parent, false);
                break;
            case 0x21:
                break;
        }

        ChatMessageAdapter.ViewHolder holder = new ChatMessageAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ChatMessageAdapter.ViewHolder viewHolder = (ChatMessageAdapter.ViewHolder) holder;
        final Chat.Message message = this.messages.get(position);
        colorStatus = "#666666";

        if(selectedMessages.contains(message)){
            viewHolder.itemView
                    .setBackground(ContextCompat.getDrawable(context, drawable.background_message_selected));
            View view = viewHolder.itemView.findViewById(id.messageLayout);
            if(currentUserIsSender(message)) {
                BackgroundHelper.setBackgroundDrawable(view.getBackground(), Color.argb(100, 220, 248, 198));
            }
            else{
                BackgroundHelper.setBackgroundDrawable(view.getBackground(), Color.argb(100, 255, 255, 255));
            }
        }
        else{
            viewHolder.itemView
                    .setBackground(ContextCompat
                            .getDrawable(context, drawable.transparent));
            View view = viewHolder.itemView.findViewById(id.messageLayout);

            if(currentUserIsSender(message)) {
                BackgroundHelper.setBackgroundDrawable(view.getBackground(),
                        ContextCompat.getColor(this.context, R.color.greenMessage));
            }
            else{
                BackgroundHelper.setBackgroundDrawable(view.getBackground(),
                         Color.argb(255, 255, 255, 255));
            }
        }

        switch (message.getType()) {
            case "text":
                
                viewHolder.content.setText(Html.fromHtml(message.getContent()));
                
                if (viewHolder.content.getText().length() > 33 || viewHolder.content.getLineCount() > 1) {
                    
                    RelativeLayout.LayoutParams params =
                            (RelativeLayout.LayoutParams) viewHolder.date.getLayoutParams();
                    params.removeRule(RelativeLayout.ALIGN_BOTTOM);
                    params.removeRule(RelativeLayout.RIGHT_OF);
                    params.addRule(RelativeLayout.BELOW, id.txtHtml);
                    params.addRule(RelativeLayout.ALIGN_RIGHT, id.txtHtml);
                    
                }
                
                FirebaseDataSource.getInstance().setChatMessageAsRead(message, chatId);
                
                break;
            case "image":
                
                colorStatus = "#FFFFFF";
                viewHolder.subtitle.setText(message.getSubtitle());
                viewHolder.uploadProgress.setVisibility(View.VISIBLE);
                
                try {
                    Glide.with(context)
                          .load(drawable.image_placeholder)
                          .asBitmap()
                          .error(drawable.image_placeholder)
                          .diskCacheStrategy(DiskCacheStrategy.ALL)
                          .into(viewHolder.imageMessage);
                }
                catch(Exception ex){
                    //TODO Programar exceção
                }
                
                if(currentUserIsSender(message) && message.getPath() != null){

                    final File source = new File(message.getPath());
                    if(message.isUploaded()) {
                        viewHolder.uploadProgress.setVisibility(View.GONE);
                        Glide.with(context)
                        .load(source)
                        .asBitmap()
                        .error(drawable.image_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.imageMessage);
                    }
                    else{
                        viewHolder.uploadProgress.setVisibility(View.VISIBLE);
                        FirebaseDataSource.getInstance().uploadFileChatMessage(chatId, message.getMessageId(), message);
                    }
                }
                else{
                    final File source = ImageHelper.getDownloadFilePath("Chats", message.getContent());
                    if(source.exists()) {
                        viewHolder.uploadProgress.setVisibility(View.GONE);
                        FirebaseDataSource.getInstance().setChatMessageAsRead(message, chatId);
                        Glide.with(context)
                        .load(source)
                        .asBitmap()
                        .error(drawable.image_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.imageMessage);
                    }
                    else {

                        if(message.getType().equals("image")){

                            String firebaseStoragePath = context.getString(string.firebase_storage);
                            StorageReference storageRef = FirebaseStorage.getInstance()
                                    .getReferenceFromUrl(firebaseStoragePath);

                            storageRef
                                .child("users")
                                .child(FirebaseDataSource.getInstance().getUser().getUserId())
                                .child("chats")
                                .child(chatId)
                                .child(message.getMessageId())
                                .child(message.getContent())
                                .getFile(ImageHelper.getDownloadFilePath("Chats", message.getContent()))
                            .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                Glide.with(context)
                                .load(source)
                                .asBitmap()
                                .error(drawable.image_placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(viewHolder.imageMessage);
                                viewHolder.uploadProgress.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                }
                break;
        }

        if(message.getDate() != null) {
            Date date = new Date(message.getDate());
            Date date1 = DateHelper.getDateOnly(date).getTime();
            Date date2 = DateHelper.getDateOnly(new Date()).getTime();

            if (date1.compareTo(date2) == 0) {
                SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
                viewHolder.date.setText(formatTime.format(date));
            } else {
                SimpleDateFormat formatDay = new SimpleDateFormat("dd/MM");
                viewHolder.date.setText(formatDay.format(date));
            }

            if (viewHolder.status != null) {
                Typeface iconFont = FontManager.getTypeface(this.context, FontManager.IONICONS);
                FontManager.markAsIconContainer(viewHolder.status, iconFont);

                if (message.isSent()) {
                    viewHolder.status.setText(this.context.getString(string.ion_android_done));
                    viewHolder.status.setTextColor(Color.parseColor(colorStatus));
                } else {
                    viewHolder.status.setText(this.context.getString(string.ion_android_alarm_clock));
                    viewHolder.status.setTextColor(Color.parseColor(colorStatus));
                }

                if (message.isReceived()) {
                    viewHolder.status.setText(this.context.getString(string.ion_android_done_all));
                    viewHolder.status.setTextColor(Color.parseColor("#666666"));
                }

                if (message.isRead()) {
                    viewHolder.status.setText(this.context.getString(string.ion_android_done_all));
                    viewHolder.status.setTextColor(Color.parseColor("#58c6f4"));
                }

            }

            viewHolder.itemView.setVisibility(View.VISIBLE);
        }
    }

    public boolean currentUserIsSender(Chat.Message message) {
        return message.getSenderId().equals(FirebaseDataSource.getInstance().getUser().getUserId());
    }

    @Override
    public int getItemCount() {
        return (messages == null? 0 : messages.size());
    }

    @Override
    public int getItemViewType(int position) {
        Chat.Message message = this.messages.get(position);
        if (currentUserIsSender(message)) {
            switch (message.getType()) {
                case "text":
                    return 0x00;
                case "image":
                    return 0x10;
                default:
                    return 0x20;
            }
        } else {
            switch (message.getType()) {
                case "text":
                    return 0x01;
                case "image":
                    return 0x11;
                default:
                    return 0x21;
            }
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView content;
        private TextView date;
        private TextView status;
        private ImageView imageMessage;
        private TextView subtitle;
        private ProgressBar uploadProgress;

        public ViewHolder(View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(id.txtHtml);
            date = (TextView) itemView.findViewById(id.txtDate);
            status = (TextView) itemView.findViewById(id.txtStatus);
            imageMessage = (ImageView) itemView.findViewById(id.imageMessage);
            subtitle = (TextView) itemView.findViewById(id.txtSubtitle);
            uploadProgress = (ProgressBar) itemView.findViewById(id.uploadProgress);
            itemView.setVisibility(View.GONE);
        }

    }

}
