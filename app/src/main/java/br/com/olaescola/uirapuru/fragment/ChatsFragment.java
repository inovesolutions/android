package br.com.olaescola.uirapuru.fragment;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.easyandroidanimations.library.FadeInAnimation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.activity.ChatMessagesActivity;
import br.com.olaescola.uirapuru.adapter.ChatAdapter;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.listener.RecyclerItemClickListener;
import br.com.olaescola.uirapuru.model.Chat;

public class ChatsFragment extends Fragment
        implements RecyclerItemClickListener.OnItemClickListener,
        FirebaseDataSource.OnChatsChangedListener{


    private static final int PERMISSION_CAMERA = 0x300;
    private RecyclerView recycler;
    private AHBottomNavigation bottomNavigation;
    private List<Chat> selectedChats;
    private ChatAdapter adapter;
    private MenuItem deleteMenuItem;
    private LinearLayoutManager layout;
    private Chat chatSelected;

    public ChatsFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chats, container, false);
        recycler = (RecyclerView) view.findViewById(R.id.recycler);

        selectedChats = new ArrayList<>();
        FirebaseDataSource.getInstance().setOnChatsChangedListener(this);
        layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler.setLayoutManager(layout);
        recycler.addOnItemTouchListener(new RecyclerItemClickListener(this.getContext(), this));
        adapter = new ChatAdapter(new ArrayList<Chat>(), selectedChats, this.getActivity(), layout);
        recycler.setAdapter(adapter);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.chats_menu, menu);
        deleteMenuItem = menu.findItem(R.id.delete);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                removeChats();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        new FadeInAnimation(recycler).setDuration(750).animate();
        FirebaseDataSource.getInstance().setOnChatsChangedListener(this);

        reloadChats();

        int qtdeNotRead = 0;
        for(Chat chat : adapter.getChats()) {
            qtdeNotRead += chat.getBadge();
        }
        if (qtdeNotRead > 0) {
            bottomNavigation.setNotification(String.valueOf(qtdeNotRead), 0);
        } else {
            bottomNavigation.setNotification("", 0);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FirebaseDataSource.getInstance().setOnChatsChangedListener(null);
    }

    private void removeChats() {

        FirebaseDataSource.getInstance().removeChats(selectedChats);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setTitle(R.string.default_title);
        deleteMenuItem.setVisible(false);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.bottom_navigation);
    }

    @Override
    public void onItemClick(View childView, int position) {

        if (selectedChats.size() > 0) {
            toogleItemSelected(position);
            return;
        }

        Intent intent = new Intent(getActivity(), ChatMessagesActivity.class);
        Bundle b = new Bundle();
        chatSelected = adapter.getChats().get(position);
        b.putString("chatId", chatSelected.getContactId());
        intent.putExtras(b);

        getActivity().startActivity(intent);
    }

    private void toogleItemSelected(int position) {

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Chat chat = adapter.getChats().get(position);

        if (selectedChats.contains(chat)) {
            selectedChats.remove(chat);
        } else {
            selectedChats.add(chat);
        }
        
        adapter.notifyItemRangeChanged(0, adapter.getChats().size());

        if (selectedChats.size() > 0) {
            activity.getSupportActionBar().setTitle(R.string.deleting_chats);
            deleteMenuItem.setVisible(true);
        } else {
            activity.getSupportActionBar().setTitle(R.string.default_title);
            deleteMenuItem.setVisible(false);
        }
    }

    @Override
    public void onItemLongPress(View childView, int position) {
        Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(50);
        toogleItemSelected(position);
    }
    
    @Override
    public void onChanged(Chat chat) {
        reloadChats();
    }
    
    @Override
    public void onAdded(Chat chat) {
/*        if(chatSelected != null && !chat.getContactId().equals(chatSelected.getContactId())){
            if( PreferenceManager
                    .getDefaultSharedPreferences(this.getContext().getApplicationContext())
                    .getBoolean("switchSoundMessage", true)) {
                MediaPlayer mp = MediaPlayer.create(this.getContext(), R.raw.nova_mensagem);
                mp.start();
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            }
        }*/
        reloadChats();
    }
    @Override
    public void onRemoved(Chat chat) {
        reloadChats();
    }

    private void reloadChats() {
        if(adapter != null) {
            if(FirebaseDataSource.getInstance().getChats() != null) {
    
                int qtdeNotRead = 0;
    
                for(Chat chat: FirebaseDataSource.getInstance().getChats()){
                    int position = adapter.getChats().indexOf(chat);
                    if(chat.getMessages().size() > 0){
                        if(position > -1) {
                            adapter.getChats().set(position, chat);
                            adapter.notifyItemChanged(position);
                        }
                        else{
                            adapter.getChats().add(chat);
                        }
                    }
                    else {
                        if(position > -1){
                            adapter.getChats().remove(position);
                            recycler.removeViewAt(position);
                            adapter.notifyItemRemoved(position);
                            adapter.notifyItemRangeChanged(position, adapter.getChats().size());
                        }
                    }
    
                    qtdeNotRead += chat.getBadge();
                    Collections.sort(adapter.getChats(), new Comparator<Chat>() {
                        @Override
                        public int compare(Chat o1, Chat o2) {
                            return o1.compareTo(o2.getLastChange());
                        }
                    });
                    adapter.notifyDataSetChanged();
    
                }

                if (qtdeNotRead > 0) {
                    bottomNavigation.setNotification(String.valueOf(qtdeNotRead), 0);
                } else {
                    bottomNavigation.setNotification("", 0);
                }
            }
        }
    }

}
