package br.com.olaescola.uirapuru.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.fragment.InboxFragment;

public class MessagesActivity extends AppCompatActivity{

    private String moduleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        moduleId = getIntent().getStringExtra("moduleId");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        InboxFragment fragment = new InboxFragment();
        fragment.setModuleId(moduleId);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_notices, fragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }

}
