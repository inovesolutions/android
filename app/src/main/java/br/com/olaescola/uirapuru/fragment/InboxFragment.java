package br.com.olaescola.uirapuru.fragment;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.easyandroidanimations.library.FadeInAnimation;

import java.util.ArrayList;
import java.util.List;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.activity.MessageActivity;
import br.com.olaescola.uirapuru.adapter.InboxAdapter;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.listener.RecyclerItemClickListener;
import br.com.olaescola.uirapuru.model.Message;

public class InboxFragment extends Fragment
        implements RecyclerItemClickListener.OnItemClickListener,
        FirebaseDataSource.OnMessagesChangedListener {

    private static final int PERMISSION_CAMERA = 0x300;
    private RecyclerView recycler;
    private FirebaseDataSource dataSource;
    private LinearLayoutManager layout;
    private AHBottomNavigation bottomNavigation;
    private InboxAdapter adapter;
    private List<Message> selectedMessages;
    private MenuItem deleteMenuItem;
    private String moduleId;

    public InboxFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        dataSource = FirebaseDataSource.getInstance();

        layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler.setLayoutManager(layout);
        dataSource.setOnMessagesChangedListener(this);
        selectedMessages = new ArrayList<>();
        adapter = new InboxAdapter(new ArrayList<Message>(), selectedMessages, layout, this.getActivity());
        recycler.addOnItemTouchListener(new RecyclerItemClickListener(this.getContext(), this));
        recycler.setAdapter(adapter);
        
        return view;

    }

    @Override
    public void onPause() {
        super.onPause();
        dataSource.setOnMessagesChangedListener(null);
        for(Message message: adapter.getMessages()){
            if(message.isDeleted()) message.setDeleted(false);
        }
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.bottom_navigation);
    }

    @Override
    public void onItemClick(View childView, int position) {
        if (selectedMessages.size() > 0) {
            toogleItemSelected(position);
            return;
        }

        Intent intent = new Intent(getActivity(), MessageActivity.class);
        Bundle b = new Bundle();
        b.putString("messageId", adapter.getMessages().get(position).getMessageId());
        intent.putExtras(b);

        getActivity().startActivity(intent);
    }

    @Override
    public void onItemLongPress(View childView, int position) {
        Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(50);
        toogleItemSelected(position);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.chats_menu, menu);
        deleteMenuItem = menu.findItem(R.id.delete);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                removeMessages();
                break;
            default:
                this.getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void removeMessages(){
        
        FirebaseDataSource.getInstance().removeMessages(selectedMessages);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setTitle(R.string.default_title);
        selectedMessages.clear();
        deleteMenuItem.setVisible(false);
        
    }

    private void toogleItemSelected(int position) {

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Message message = adapter.getMessages().get(position);

        if (selectedMessages.contains(message)) {
            selectedMessages.remove(message);
            message.setDeleted(false);
        } else {
            selectedMessages.add(message);
            message.setDeleted(true);
        }
    
        adapter.notifyItemRangeChanged(0, adapter.getMessages().size());
    
        if (selectedMessages.size() > 0) {
            activity.getSupportActionBar().setTitle(R.string.deleting_notices);
            deleteMenuItem.setVisible(true);
        } else {
            activity.getSupportActionBar().setTitle(R.string.default_title);
            deleteMenuItem.setVisible(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new FadeInAnimation(recycler).setDuration(750).animate();
        dataSource.setOnMessagesChangedListener(this);

        if(moduleId != null){
            List<Message> messages = dataSource.getMessages(moduleId);
            if(messages != null) {
                adapter.setMessages(messages);
            }
        }
        else {
            adapter.setMessages(dataSource.getMessages());
        }
        
        adapter.notifyDataSetChanged();
        updateBadge();
    
    }
    
    public void updateBadge() {
        
        if(bottomNavigation != null) {

            int qtdeNotRead = 0;
            for (Message message : adapter.getMessages()) {
                qtdeNotRead += (!message.isRead() ? 1 : 0);
            }
            if (qtdeNotRead > 0) {
                bottomNavigation.setNotification(String.valueOf(qtdeNotRead), 1);
            } else {
                bottomNavigation.setNotification("", 1);
            }
        }
    }
    
    @Override
    public void onChanged(Message message) {
        reloadMessages();
    }
    
    @Override
    public void onAdded(Message message) {
        reloadMessages();
    }
    
    @Override
    public void onRemoved(Message message) {
        reloadMessages();
    }
    
    public void reloadMessages() {
        List<Message> messages;
        if(moduleId != null){
            messages = dataSource.getMessages(moduleId);
            if(messages != null) {
                adapter.setMessages(messages);
            }
        }
        else {
            adapter.setMessages(dataSource.getMessages());
        }
        
        adapter.notifyDataSetChanged();
        updateBadge();
    }
}
