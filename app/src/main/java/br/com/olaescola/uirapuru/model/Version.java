package br.com.olaescola.uirapuru.model;

/**
 * Created by developer on 24/08/17.
 */

public class Version {
   private String android;
   private String ios;
   private String web;
   
   public String getAndroid() {
      return android;
   }
   
   public void setAndroid(String android) {
      this.android = android;
   }
   
   public String getIos() {
      return ios;
   }
   
   public void setIos(String ios) {
      this.ios = ios;
   }
   
   public String getWeb() {
      return web;
   }
   
   public void setWeb(String web) {
      this.web = web;
   }
}
