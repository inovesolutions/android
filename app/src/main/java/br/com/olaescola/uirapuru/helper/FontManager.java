package br.com.olaescola.uirapuru.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FontManager {

    public static final String ROOT = "fonts/";
    public static final String FONTAWESOME = ROOT + "fontawesome-webfont.ttf";
    public static final String IONICONS = ROOT + "ionicons.ttf";
    public static final String WORK_SANS = ROOT + "WorkSans-Regular.ttf";
    public static final String WORK_SANS_BOLD = ROOT + "WorkSans-Bold.ttf";
    public static final String WORK_SANS_EXTRA_BOLD = ROOT + "WorkSans-ExtraBold.ttf";

    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }

    public static void markAsIconContainer(View v, Typeface typeface) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View child = vg.getChildAt(i);
                markAsIconContainer(child, typeface);
            }
        } else if (v instanceof TextView) {
            ((TextView) v).setTypeface(typeface);
        }
    }

}
