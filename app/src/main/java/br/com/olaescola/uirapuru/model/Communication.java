package br.com.olaescola.uirapuru.model;

/**
 * Created by developer on 18/08/17.
 */

public class Communication extends Message.Content {
   
   private String html;
   private String imageURL;
   
   public String getHtml() {
      return html;
   }
   
   public void setHtml(String html) {
      this.html = html;
   }
   
   public String getImageURL() {
      return imageURL;
   }
   
   public void setImageURL(String imageURL) {
      this.imageURL = imageURL;
   }
   
}
