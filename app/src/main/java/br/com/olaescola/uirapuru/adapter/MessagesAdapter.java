package br.com.olaescola.uirapuru.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.easyandroidanimations.library.ScaleInAnimation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.activity.MessagesActivity;
import br.com.olaescola.uirapuru.helper.DateHelper;
import br.com.olaescola.uirapuru.model.Message;
import br.com.olaescola.uirapuru.model.Module;

/**
 * Created by developer on 08/06/17.
 */

public class MessagesAdapter extends Adapter {
    private final LinearLayoutManager layout;
    private List<Module> modules;
    private Context context;
    private Date lastMessageDate;

    public MessagesAdapter(List<Module> modules, Context context, LinearLayoutManager layout) {
        this.modules = modules;
        this.context = context;
        this.layout = layout;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.messages_item, parent, false);
        MessagesAdapter.ViewHolder holder = new MessagesAdapter
                .ViewHolder(view, this.context, this);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        
        MessagesAdapter.ViewHolder viewHolder = (ViewHolder) holder;
        Module module = this.modules.get(position);
        viewHolder.setModule(module);
        viewHolder.toName.setText(module.getName());
        viewHolder.itemView.setOnClickListener(viewHolder);

        if (module.getBadge() > 0) {
            new ScaleInAnimation(viewHolder.badge).setDuration(300).animate();
            viewHolder.badge.setVisibility(View.VISIBLE);
            viewHolder.badge.setText(String.valueOf(module.getBadge()));
        } else {
            viewHolder.badge.setVisibility(View.GONE);
        }

        List<Message> messages = module.getMessages();

        if(messages != null && messages.size() > 0){

            Message message = messages.get(messages.size() - 1);
            SimpleDateFormat format = new SimpleDateFormat(this.context.getString(R.string.date_pattern));
            Date date = null;

            try {
                date = format.parse(message.getContent().getHeader().getDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (lastMessageDate instanceof Date) {
                if (date.getTime() >= lastMessageDate.getTime()) {
                    lastMessageDate = date;
                }
            } else {
                lastMessageDate = date;
            }

            try {
                Date dateTime = new SimpleDateFormat(this.context.getString(R.string.date_pattern_mili))
                        .parse(message.getContent().getHeader().getDate());
                viewHolder.lastUpdate.setText(DateHelper
                      .formatToYesterdayOrToday(dateTime));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public int getItemCount() {
        return modules.size();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView toName;
        private final TextView lastUpdate;
        private final TextView lastMessage;
        private final ImageView avatar;
        private final Context context;
        private final MessagesAdapter adapter;
        private final TextView badge;
        private Module module;

        public ViewHolder(View itemView, Context context, MessagesAdapter adapter) {
            super(itemView);
            this.context = context;
            this.toName = (TextView) itemView.findViewById(R.id.txtName);
            this.lastMessage = (TextView) itemView.findViewById(R.id.txtLastMessage);
            this.lastUpdate = (TextView) itemView.findViewById(R.id.txtLastUpdate);
            this.avatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
            this.badge = (TextView) itemView.findViewById(R.id.txtBadge);
            this.adapter = adapter;

            this.itemView.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, MessagesActivity.class);
            Bundle b = new Bundle();
            b.putString("moduleId", module.getModuleId());
            intent.putExtras(b);
            ((Activity) context).startActivity(intent);
        }

        public Module getModule() {
            return module;
        }

        public void setModule(Module module) {
            this.module = module;
        }
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> newModules) {
        this.modules.clear();
        this.modules.addAll(newModules);
        this.layout.removeAllViews();
        Collections.sort(this.modules, new Comparator<Module>() {
            @Override
            public int compare(Module o1, Module o2) {
                return o1.compareTo(o2);
            }
        });

        this.notifyDataSetChanged();
    }
}
