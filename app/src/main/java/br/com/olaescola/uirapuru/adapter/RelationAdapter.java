package br.com.olaescola.uirapuru.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;

import java.util.List;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.helper.FontManager;
import br.com.olaescola.uirapuru.model.Relation;

/**
 * Created by developer on 30/08/17.
 */

public class RelationAdapter extends RecyclerView.Adapter{
   
   private List<Relation> relations;
   private Context context;
   
   public RelationAdapter(List<Relation> relations, Context context){
      this.relations = relations;
      this.context = context;
      
   }
   @Override
   public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(context).inflate(R.layout.relation_item, parent, false);
      RelationAdapter.ViewHolder holder = new RelationAdapter.ViewHolder(view, this.context);
      return holder;
   }
   
   @Override
   public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
      final RelationAdapter.ViewHolder viewHolder = (ViewHolder) holder;
      final Relation relation = this.relations.get(position);
      
      viewHolder.txtName.setText(relation.getProfile().getDisplayName());
      
      StringBuilder types = new StringBuilder();
      for (String type : relation.getTypes()) {
         types.append(type).append(" ");
      }
      
      viewHolder.txtTypes.setText(types);
      
      if(relation.getProfile().getAvatar() != null){
         try {
            Glide.with(context)
                  .using(new FirebaseImageLoader())
                  .load(FirebaseStorage
                          .getInstance()
                          .getReferenceFromUrl(context.getString(R.string.firebase_storage_avatars))
                          .child(relation.getProfile().getProfileId())
                          .child(relation.getProfile().getAvatar()))
                  .fallback(R.drawable.placeholder_user)
               .into(viewHolder.imgAvatar);
         }
         catch(Exception ex){
            ex.printStackTrace();
         }
      }
   }
   
   @Override
   public int getItemCount() {
      return relations.size();
   }
   
   public void setRelations(List<Relation> relations) {
      if(this.relations != null) this.relations.clear();
      this.relations.addAll(relations);
      this.notifyDataSetChanged();
   }
   
   private class ViewHolder extends RecyclerView.ViewHolder{
   
      private final Context context;
      private TextView txtName;
      private TextView txtTypes;
      private ImageView imgAvatar;
      private View itemView;
      
      public ViewHolder(View itemView, Context context) {
         super(itemView);
         this.context = context;
         this.itemView = itemView;
         this.txtName = (TextView) itemView.findViewById(R.id.txtName);
         Typeface font = FontManager.getTypeface(context, FontManager.WORK_SANS_BOLD);
         FontManager.markAsIconContainer(txtName, font);
         this.txtTypes = (TextView) itemView.findViewById(R.id.txtTypes);
         this.imgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
         
         this.txtName.setText("");
         this.txtTypes.setText("");
      }
   }
}
