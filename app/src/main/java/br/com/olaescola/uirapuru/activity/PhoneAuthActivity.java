package br.com.olaescola.uirapuru.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.florent37.materialtextfield.MaterialTextField;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import br.com.olaescola.uirapuru.R;

public class PhoneAuthActivity extends AppCompatActivity {
   
   private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";
   
   private boolean mVerificationInProgress = false;
   private EditText edtPhoneAuth;
   private boolean inProgress = false;
   private TextView txtMessageLog;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_phone_auth);
      getSupportActionBar().hide();
      
      edtPhoneAuth = (EditText) this.findViewById(R.id.edtPhoneAuth);
      edtPhoneAuth.setRawInputType(Configuration.KEYBOARD_12KEY);
      edtPhoneAuth.setOnEditorActionListener(new PhoneNumberVerificator());
      txtMessageLog = (TextView) this.findViewById(R.id.txtMessageLog);
   }
   
   private boolean validatePhoneNumber() {
      String phoneNumber = edtPhoneAuth.getText().toString();
      if (TextUtils.isEmpty(phoneNumber)) {
         edtPhoneAuth.setError(getString(R.string.AUTH_PHONE_EMPTY));
         return false;
      }
      
      return true;
   }
   
   private void startPhoneNumberVerification(String phoneNumber) {
      
      RequestQueue requestQueue = Volley.newRequestQueue(this);
      Map<String, String> params = new HashMap<>();
      params.put("phoneNumber", phoneNumber);

      txtMessageLog.setText("Estamos verificando seu cadastro agora...");
      JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
            getString(R.string.cloudUrl) + "/verifyPhoneNumber",
            new JSONObject(params),
            new Response.Listener<JSONObject>() {
               @Override
               public void onResponse(JSONObject response) {
                  try{
                     String credentials = response.getString("credentials");
                     Intent intent = new Intent(PhoneAuthActivity.this, CodeAuthActivity.class);
                     Bundle b = new Bundle();
                     b.putString("credentials", credentials);
                     intent.putExtras(b);
                     PhoneAuthActivity.this.startActivityForResult(intent, 0);
                  }
                  catch(JSONException ex){
                     txtMessageLog.setText(R.string.login_message);
                     Toast.makeText(PhoneAuthActivity.this, "Erro ao ler a resposta do servidor", Toast.LENGTH_LONG).show();
                  }
                  inProgress = false;
                  txtMessageLog.setText(R.string.login_message);
               }
            },
            new Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {
                  txtMessageLog.setText(R.string.login_message);
                  if(error.networkResponse == null || error.networkResponse.statusCode == 500){
                     Toast.makeText(PhoneAuthActivity.this, "Erro ao processar a requisição", Toast.LENGTH_LONG).show();
                  }
                  else if(error.networkResponse.statusCode == 404){
                     Toast.makeText(PhoneAuthActivity.this,
                           "O número de celular informado não foi encontrado em nossa base de dados",
                           Toast.LENGTH_LONG).show();
                  }
                  else if(error.networkResponse.statusCode == 400){
                     Toast.makeText(PhoneAuthActivity.this,
                           "Não conseguimos enviar o SMS, por favor tente novamente.",
                           Toast.LENGTH_LONG).show();
                  }
   
                  inProgress = false;
   
               }
            });
      
      request.setRetryPolicy(new DefaultRetryPolicy(0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
      
      requestQueue.add(request);
   }
   
   @Override
   public void onBackPressed() {
      Bundle bundle = new Bundle();
      Intent mIntent = new Intent();
      mIntent.putExtras(bundle);
      setResult(RESULT_OK, mIntent);
      finish();
   }
   
   @Override
   protected void onSaveInstanceState(Bundle outState) {
      super.onSaveInstanceState(outState);
      outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
   }
   
   @Override
   protected void onRestoreInstanceState(Bundle savedInstanceState) {
      super.onRestoreInstanceState(savedInstanceState);
      mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
   }
   
   private class PhoneNumberVerificator implements EditText.OnEditorActionListener {
      
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
         
         if (actionId != KeyEvent.ACTION_DOWN || inProgress) return false;
         inProgress = true;
         try {
            
            if (validatePhoneNumber()) {
               final String phoneNumber = edtPhoneAuth.getText().toString();
               startPhoneNumberVerification(phoneNumber);
            } else {
               Snackbar
                     .make(findViewById(R.id.phoneAuthLayout), R.string.wrong_phone_message, Snackbar.LENGTH_LONG)
                     .show();
            }
         } catch (Exception ex) {
            ex.printStackTrace();
         }
         return true;
      }
   }
   
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      if (requestCode == 0) {
         if (resultCode == RESULT_OK) {
            finish();
         }
      }
   }
}
