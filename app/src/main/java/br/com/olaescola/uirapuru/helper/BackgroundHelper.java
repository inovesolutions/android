package br.com.olaescola.uirapuru.helper;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;

/**
 * Created by developer on 31/08/17.
 */

public class BackgroundHelper {
   
   public static void setBackgroundDrawable(Drawable background, int color){
      if (background instanceof ShapeDrawable) {
         // cast to 'ShapeDrawable'
         ShapeDrawable shapeDrawable = (ShapeDrawable) background;
         shapeDrawable.getPaint().setColor(color);
      } else if (background instanceof GradientDrawable) {
         GradientDrawable gradientDrawable = (GradientDrawable) background;
         gradientDrawable.setColor(color);
      } else if (background instanceof ColorDrawable) {
         ColorDrawable colorDrawable = (ColorDrawable) background;
         colorDrawable.setColor(color);
      }
   }
}
