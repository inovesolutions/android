package br.com.olaescola.uirapuru.helper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import br.com.olaescola.uirapuru.MainActivity;
import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.model.Chat;

public class ChatHelper {

    private final FirebaseAuth firebaseAuth;
    private final Context context;
    private String contactId;

    private static final long THRESHOLD_MILLIS = 1000L;
    private long lastClickMillis;

    public ChatHelper(Context context) {
        this.context = context;
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void saveMessageWithImage(final String fileName, final String subtitle, final String imagePath) {
        long now = SystemClock.elapsedRealtime();
        if (now - lastClickMillis > THRESHOLD_MILLIS) {
            Chat.Message message = new Chat.Message();
            message.setType("image");
            message.setSent(true);
            message.setRead(false);
            message.setReceived(false);
            message.setContent(fileName);
            message.setSubtitle(subtitle);
            message.setPath(imagePath);
            message.setDeleted(false);
            message.setUploaded(false);
            DatabaseReference reference = FirebaseDataSource.getInstance().saveChatMessage(message, contactId);
            
        }

        lastClickMillis = now;

    }

    public void saveMessageWithHtml(final TextView view) {

        long now = SystemClock.elapsedRealtime();
        if (now - lastClickMillis > THRESHOLD_MILLIS) {

            String content = view.getText().toString();
            if (content.isEmpty()) return;

            Chat.Message message = new Chat.Message();
            message.setType("text");
            message.setSent(true);
            message.setRead(false);
            message.setReceived(false);
            message.setDeleted(false);
            message.setContent(content);

            FirebaseDataSource.getInstance().saveChatMessage(message, contactId);

            view.setText("");
        }

        lastClickMillis = now;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }
}
