package br.com.olaescola.uirapuru.service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class OlaEscolaFirebaseInstanceIdService extends FirebaseInstanceIdService {
   
   private FirebaseDatabase firebaseDatabase;
   private FirebaseAuth firebaseAuth;
   
   public OlaEscolaFirebaseInstanceIdService() {
      firebaseDatabase = FirebaseDatabase.getInstance();
      firebaseAuth = FirebaseAuth.getInstance();
   }
   
   @Override
   public void onTokenRefresh() {
      String refreshedToken = FirebaseInstanceId.getInstance().getToken();
      if (firebaseAuth.getCurrentUser() != null) {
         firebaseDatabase.getReference().child("users")
               .child(firebaseAuth.getCurrentUser().getUid())
               .child("pushId").setValue(refreshedToken);
         firebaseDatabase.getReference().child("users")
                 .child(firebaseAuth.getCurrentUser().getUid()).child("profile")
                 .child("pushId").setValue(refreshedToken);
      }
   }
}
