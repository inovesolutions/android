package br.com.olaescola.uirapuru.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.stfalcon.multiimageview.MultiImageView;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.helper.BackgroundHelper;
import br.com.olaescola.uirapuru.helper.DateHelper;
import br.com.olaescola.uirapuru.helper.FontManager;
import br.com.olaescola.uirapuru.helper.ImageHelper;
import br.com.olaescola.uirapuru.model.Message;
import br.com.olaescola.uirapuru.model.Relation;

/**
 * Created by rafae on 18/07/2017.
 */

public class InboxAdapter extends RecyclerView.Adapter {

    private final List<Message> messages;
    private final List<Message> selectedMessages;
    private final LinearLayoutManager layout;
    private final Context context;

    public InboxAdapter(List<Message> messages, List<Message> selectedMessages,
                        LinearLayoutManager layout, Context context){
        this.messages = messages;
        this.selectedMessages = selectedMessages;
        this.layout = layout;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inbox_item, parent, false);
        InboxAdapter.ViewHolder holder = new InboxAdapter.ViewHolder(view, this.context);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        
        final InboxAdapter.ViewHolder viewHolder = (InboxAdapter.ViewHolder) holder;
        final Message message = this.messages.get(position);
        
        viewHolder.message = message;
        viewHolder.txtTitle.setText(message.getContent().getBody().getTitle());
        Typeface iconFont = FontManager.getTypeface(context, FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(viewHolder.iconMessage, iconFont);
        
        String icon = new String(Character.toChars(Integer.parseInt(
              message.getContent().getHeader().getModuleIcon(), 16)));
        
        viewHolder.iconMessage.setText(icon);
        viewHolder.iconMessage.setTextColor(message.getContent().getHeader().getModuleIconColor().getValue());
        viewHolder.txtMessage.setText(Html.fromHtml(message.getContent().getBody().getSubtitle()));
                
    
        Drawable background = viewHolder.iconMessage.getBackground();
        BackgroundHelper.setBackgroundDrawable(background, message.getContent().getHeader().getModuleIconBackground().getValue());
        viewHolder.iconMessage.setVisibility(View.VISIBLE);
    
        try {
            if (selectedMessages.contains(message)) {
                viewHolder.itemView
                      .setBackground(ContextCompat.getDrawable(context, R.drawable.background_message_selected));
            } else {
                if(message.isRead()) {
                    viewHolder.itemView
                          .setBackground(ContextCompat
                                .getDrawable(context, R.drawable.white));
                }
                else{
                    viewHolder.itemView
                          .setBackground(ContextCompat
                                .getDrawable(context, R.drawable.background_unread));
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        if(message.isRead()){
            viewHolder.txtMessage.setTypeface(null, Typeface.NORMAL);
        }
        else{
            viewHolder.txtMessage.setTypeface(null, Typeface.BOLD);
        }
    
        try {
            Date dateTime = new SimpleDateFormat(this.context.getString(R.string.date_pattern_mili))
                    .parse(message.getContent().getHeader().getDate());
            viewHolder.txtUpdate.setText(DateHelper
                  .formatToYesterdayOrToday(dateTime));
        }
        catch(ParseException exception){
            System.out.println(exception.getMessage());
        }

        if(message.getContent().getHeader().getRelations() != null){
            viewHolder.imgRelationAvatar.setVisibility(View.VISIBLE);
            viewHolder.imgRelationAvatar.clear();
            Iterator<Map.Entry<String, Relation>> iterator = message
                    .getContent().getHeader().getRelations().entrySet().iterator();

            while(iterator.hasNext()){
                Relation relation = iterator.next().getValue();
                final File avatarRelation = ImageHelper.getDownloadFilePath("Avatars", relation.getProfile().getAvatar());
                if(avatarRelation.exists()){
                    Bitmap bitmap = BitmapFactory.decodeFile(avatarRelation.getPath());
                    if(bitmap != null) {
                        viewHolder.imgRelationAvatar.addImage(bitmap);
                    }
                }
                else{
                    if(relation.getProfile().getAvatar() != null) {
                        try {
                            FirebaseStorage.getInstance()
                            .getReferenceFromUrl(context
                            .getString(R.string.firebase_storage_avatars)
                            .concat("/")
                            .concat(relation.getProfile().getProfileId())
                            .concat("/")
                            .concat(relation.getProfile().getAvatar()))
                            .getFile(avatarRelation)
                            .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    Bitmap bitmap = BitmapFactory.decodeFile(avatarRelation.getPath());
                                    if(bitmap != null) viewHolder.imgRelationAvatar.addImage(bitmap);
                                }
                            });
                        }
                        catch(Exception ex){
                            System.out.println(ex.getMessage());
                            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                                    R.drawable.placeholder_user);
                            viewHolder.imgRelationAvatar.addImage(bitmap);
                        }
                    }
                    else{
                        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.placeholder_user);
                        viewHolder.imgRelationAvatar.addImage(bitmap);
                    }
                }
            }

        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void setMessages(List<Message> newMessages) {
    
        messages.retainAll(newMessages);
        
        for (Message newMessage: newMessages){
            boolean updated = false;
            int index = 0;
            for(Message oldMessage: this.messages){
                if(oldMessage.equals(newMessage)){
                    this.messages.set(index, newMessage);
                    updated = true;
                }
                else index++;
            }
            if (!updated) this.messages.add(newMessage);
        }

        Collections.sort(this.messages, new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return o1.compareTo(o2);
            }
        });

        List<Message> removedMessages = new ArrayList<>();
        for(Message message: messages){
            if(message.isDeleted()) removedMessages.add(message);
        }
        
        messages.removeAll(removedMessages);
        this.notifyDataSetChanged();

    }

    public List<Message> getMessages() {
        return messages;
    }

    private class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txtTitle;
        private TextView txtUpdate;
        private TextView txtMessage;
        private ImageView imgSenderAvatar;
        private MultiImageView imgRelationAvatar;
        private TextView iconMessage;
        private Context context;

        private Message message;

        public ViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            this.txtTitle = (TextView) itemView.findViewById(R.id.txtName);
            Typeface txtFont = FontManager.getTypeface(context, FontManager.WORK_SANS_BOLD);
            FontManager.markAsIconContainer(this.txtTitle, txtFont);
            this.txtMessage = (TextView) itemView.findViewById(R.id.txtLastMessage);
            this.txtUpdate = (TextView) itemView.findViewById(R.id.txtLastUpdate);
            this.imgSenderAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
            this.imgRelationAvatar = (MultiImageView) itemView.findViewById(R.id.imgParent);
            this.imgRelationAvatar.setShape(MultiImageView.Shape.CIRCLE);
            this.iconMessage = (TextView) itemView.findViewById(R.id.txtIcon);

            this.txtTitle.setText("");
            this.txtMessage.setText("");
            this.txtUpdate.setText("");
            this.imgRelationAvatar.setVisibility(View.GONE);
            this.iconMessage.setVisibility(View.GONE);
        }
    }
}
