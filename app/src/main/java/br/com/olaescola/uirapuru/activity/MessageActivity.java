package br.com.olaescola.uirapuru.activity;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.fragment.ImageFullScreenFragment;
import br.com.olaescola.uirapuru.helper.BackgroundHelper;
import br.com.olaescola.uirapuru.helper.DateHelper;
import br.com.olaescola.uirapuru.helper.FontManager;
import br.com.olaescola.uirapuru.helper.ImageHelper;
import br.com.olaescola.uirapuru.model.Message;

public class MessageActivity extends AppCompatActivity
      implements FirebaseDataSource.OnMessagesChangedListener {
    
    private Message message;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String messageId = getIntent().getStringExtra("messageId");
        message = FirebaseDataSource.getInstance().findMessage(messageId);
        
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            if (!message.getContent().getHeader()
                  .getModuleVersion().getAndroid().equals(version)) {
                setContentView(R.layout.activity_message);
                return;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        
        renderMessage(message);
        FirebaseDataSource.getInstance().setOnMessagesChangedListener(this);
        FirebaseDataSource.getInstance().setMessageAsRead(message);
        
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
    
    private void renderImage() {
        
        addTitle();
        addIcon();
        addDate();
        addSender();
        
        ImageView imageView = (ImageView) findViewById(R.id.imageMessage);
        ProgressBar downloadProgress = (ProgressBar) findViewById(R.id.downloadProgress);
        
        TextView txtImageDescription = (TextView) findViewById(R.id.txtImageDescription);
    
        if (txtImageDescription != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtImageDescription.setText(Html.fromHtml(message.getContent().getBody().getImageDescription(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                txtImageDescription.setText(Html.fromHtml(message.getContent().getBody().getImageDescription()));
            }
        }
        if (message.getContent().getHeader().getResources() != null
              && message.getContent().getHeader().getResources().size() > 0) {
            
            final File source = ImageHelper
                  .getDownloadFilePath("Mensagens", message.getContent().getBody().getContent());
            
            try {
                if (source.exists()) {
                    Glide.with(this)
                          .load(source)
                          .asBitmap()
                          .error(R.drawable.image_placeholder)
                          .diskCacheStrategy(DiskCacheStrategy.ALL)
                          .into(imageView);
                    downloadProgress.setVisibility(View.GONE);
                } else {

                    downloadProgress.setVisibility(View.VISIBLE);
                    String firebaseStoragePath = getString(R.string.firebase_storage)
                            .concat("/messages").concat("/").concat(message.getContent().getBody().getContent());
                    StorageReference storageRef = FirebaseStorage.getInstance()
                            .getReferenceFromUrl(firebaseStoragePath);

                    storageRef.getFile(ImageHelper.getDownloadFilePath("Mensagens", message.getContent().getBody().getContent()))
                    .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                            Map<String, Object> update = new HashMap<>();
                            update.put("downloaded", true);
                            update.put("downloadDate", ServerValue.TIMESTAMP);

                            FirebaseDatabase.getInstance()
                            .getReference("users/"
                            .concat(FirebaseDataSource.getInstance().getUser().getUserId())
                            .concat("/messages/").concat(message.getMessageId())
                            .concat("/").concat("/content/header/resources/0"))
                            .updateChildren(update);

                        }
                    });
                    
                    Glide.with(this)
                          .load(R.drawable.image_placeholder)
                          .asBitmap()
                          .into(imageView);
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File source = ImageHelper.getDownloadFilePath("Mensagens", message.getContent().getBody().getContent());
                ImageFullScreenFragment dialog = ImageFullScreenFragment.newInstance(source);
                dialog.show(MessageActivity.this.getSupportFragmentManager(), "tag");
            }
        });
    }
    
    private void renderCommunication() {
        
        addTitle();
        addDate();
        addSender();
        addIcon();
        
        TextView txtContent = (TextView) this.findViewById(R.id.txtContent);
        
        if (txtContent != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtContent.setText(Html.fromHtml(message.getContent().getBody().getContent(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                txtContent.setText(Html.fromHtml(message.getContent().getBody().getContent()));
            }
        }
        
    }
    
    private void addTitle() {
        TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        Typeface txtFont = FontManager.getTypeface(this, FontManager.WORK_SANS_BOLD);
        FontManager.markAsIconContainer(txtTitle, txtFont);
        if (txtTitle != null) {
            txtTitle.setVisibility(View.GONE);
            if (message.getContent().getBody().getTitle() != null
                  && !message.getContent().getBody().getTitle().isEmpty()) {
                txtTitle.setText(message.getContent().getBody().getTitle());
                txtTitle.setVisibility(View.VISIBLE);
            }
        }
    
        TextView txtSubTitle = (TextView) findViewById(R.id.txtSubtitle);
        if (txtSubTitle != null) {
            txtSubTitle.setVisibility(View.GONE);
            if (message.getContent().getBody().getSubtitle() != null
                  && !message.getContent().getBody().getSubtitle().isEmpty()) {
                txtSubTitle.setText(message.getContent().getBody().getSubtitle());
                txtSubTitle.setVisibility(View.VISIBLE);
            }
        }
    }
    
    private void addDate() {
        try {
            TextView txtDate = (TextView) findViewById(R.id.txtDate);
            Date dateTime = new SimpleDateFormat(getString(R.string.date_pattern_mili))
                    .parse(message.getContent().getHeader().getDate());
            txtDate.setText(DateHelper
                  .formatToYesterdayOrToday(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    
    private void addIcon() {
        
        TextView txtIcon = (TextView) findViewById(R.id.txtIcon);
        Typeface iconFont = FontManager.getTypeface(this, FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(txtIcon, iconFont);
        
        String icon = new String(Character.toChars(Integer.parseInt(
              message.getContent().getHeader().getModuleIcon(), 16)));
        
        txtIcon.setText(icon);
        txtIcon.setTextColor(message.getContent().getHeader().getModuleIconColor().getValue());
        
        Drawable background = txtIcon.getBackground();
        BackgroundHelper.setBackgroundDrawable(background, message.getContent().getHeader().getModuleIconBackground().getValue());
    }
    
    private void addSender() {
        ImageView imgSender = (ImageView) findViewById(R.id.imgAvatar);
        if (imgSender != null) {
            if (message.getContent().getHeader().getSenderAvatar() != null
                  && !message.getContent().getHeader().getSenderAvatar().isEmpty()) {
                try {
                    Glide.with(this)
                            .using(new FirebaseImageLoader())
                            .load(FirebaseStorage
                            .getInstance().getReferenceFromUrl(getString(R.string.firebase_storage_avatars))
                            .child(message.getContent().getHeader().getSenderId())
                            .child(message.getContent().getHeader().getSenderAvatar()))
                            .fallback(R.drawable.placeholder_user)
                            .into(imgSender);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    
        TextView txtName = (TextView) findViewById(R.id.txtName);
        Typeface font = FontManager.getTypeface(this, FontManager.WORK_SANS_BOLD);
        FontManager.markAsIconContainer(txtName, font);
        txtName.setText(message.getContent().getHeader().getSenderName());
    }
    
    public void renderMessage(Message message) {
        switch (message.getContent().getType()) {
            case "text":
                setContentView(R.layout.text_html_item);
                renderCommunication();
                break;
            case "image":
                setContentView(R.layout.image_item);
                renderImage();
                break;
            default:
                setContentView(R.layout.activity_message);
        }
    }
    
    @Override
    public void onChanged(Message message) {
        if (message.getMessageId().equals(this.message.getMessageId())) {
            renderMessage(message);
        }
    }
    
    @Override
    public void onAdded(Message message) {
        
    }
    
    @Override
    public void onRemoved(Message message) {
        
    }
}