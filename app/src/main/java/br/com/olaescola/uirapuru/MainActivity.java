package br.com.olaescola.uirapuru;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.mikepenz.iconics.context.IconicsContextWrapper;

import br.com.olaescola.uirapuru.activity.ChatMessagesActivity;
import br.com.olaescola.uirapuru.activity.ContactActivity;
import br.com.olaescola.uirapuru.activity.PhoneAuthActivity;
import br.com.olaescola.uirapuru.adapter.MainViewPagerAdapter;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.fragment.InboxFragment;
import br.com.olaescola.uirapuru.fragment.ChatsFragment;
import br.com.olaescola.uirapuru.fragment.OptionsFragment;
import br.com.olaescola.uirapuru.helper.ExceptionHandler;
import br.com.olaescola.uirapuru.service.SoundLogoService;

import static br.com.olaescola.uirapuru.activity.ChatMessagesActivity.RECYCLER_SCROLL_POSITION;

public class MainActivity extends AppCompatActivity{
    
    private static final int LOGIN_RETURN_CODE = 0X01;
    private static final int CONTACT_ACITVITY = 0X02;
    public static final int UPLOAD_FINISHED = 1001;
    private static final int PERMISSION_CAMERA = 0x300;
    private AHBottomNavigationViewPager viewPager;
    private FloatingActionButton floatingActionButton;
    private AHBottomNavigation bottomNavigation;
    private MainViewPagerAdapter mainViewPagerAdapter;
    private FirebaseAnalytics firebaseAnalytics;
    private FirebaseAuth firebaseAuth;
    private SharedPreferences preferences;
    private FirebaseDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_main);
        Intent intentMediaPlayer = new Intent(this.getApplicationContext(), SoundLogoService.class);
        startService(intentMediaPlayer);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() == null){ goToLogin(); }

        floatingActionButton = (FloatingActionButton) findViewById(R.id.floating_action_button);
        floatingActionButton.setVisibility(View.INVISIBLE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToContacts();
            }
        });
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        configBottomNavigation();

        viewPager = (AHBottomNavigationViewPager) findViewById(R.id.view_pager);
        String layoutMessages = preferences.getString("layoutMessages", "inbox");
        mainViewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager(), layoutMessages);
        viewPager.setAdapter(mainViewPagerAdapter);
        viewPager.setCurrentItem(1);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.CAMERA)) {
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE},
                            PERMISSION_CAMERA);

                }
            }
        }
    }

    public void goToContacts(){
        Intent intent = new Intent(this, ContactActivity.class);
        startActivityForResult(intent, CONTACT_ACITVITY);
    }

    public void goToLogin(){
        Intent intent = new Intent(this, PhoneAuthActivity.class);
        startActivityForResult(intent, LOGIN_RETURN_CODE);
    }

    private void configBottomNavigation() {

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.title_chats,
                R.drawable.ic_chat_black_24dp, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.title_messages,
                R.drawable.ic_email_black_24dp, R.color.colorPrimary);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.title_options,
                R.drawable.ic_settings_black_24dp, R.color.colorPrimary);

        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));
        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));
        bottomNavigation.setForceTint(true);
        bottomNavigation.setTranslucentNavigationEnabled(true);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setColored(true);
        bottomNavigation.setCurrentItem(1);
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));
        bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                Bundle bundle = new Bundle();
                viewPager.setCurrentItem(position);
                Fragment fragment = mainViewPagerAdapter.getItem(position);

                if(fragment instanceof ChatsFragment){
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "chat_fragment");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Tela de chats");
                }
                else if(fragment instanceof InboxFragment){
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "channel_fragment");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Tela de canais");
                }
                else if(fragment instanceof OptionsFragment){
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "options_fragment");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Tela de Opções");
                }

                firebaseAnalytics.logEvent("eventoInicial", bundle);
                toggleFloatActionButton(position);
                return true;

            }
        });

        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
            @Override
            public void onPositionChange(int y) {
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void toggleFloatActionButton(int position) {
        if (position == 0) {

            floatingActionButton.setVisibility(View.VISIBLE);
            floatingActionButton.setAlpha(0f);
            floatingActionButton.setScaleX(0f);
            floatingActionButton.setScaleY(0f);
            floatingActionButton.animate().alpha(1).scaleX(1)
                    .scaleY(1).setDuration(300)
                    .setInterpolator(new OvershootInterpolator())
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            floatingActionButton.animate().setInterpolator(new LinearOutSlowInInterpolator()).start();
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    }).start();

        } else {
            if (floatingActionButton.getVisibility() == View.VISIBLE) {
                floatingActionButton.animate().alpha(0).scaleX(0)
                        .scaleY(0).setDuration(300)
                        .setInterpolator(new LinearOutSlowInInterpolator())
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                floatingActionButton.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                                floatingActionButton.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }
                        }).start();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        
        if(FirebaseAuth.getInstance().getCurrentUser() != null) {
            dataSource = FirebaseDataSource.getInstance(FirebaseAuth.getInstance().getCurrentUser());
            dataSource.connect(getApplicationContext());
        }

        preferences.edit().remove(RECYCLER_SCROLL_POSITION).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        
        switch (requestCode) {
            case LOGIN_RETURN_CODE:
                if(FirebaseAuth.getInstance().getCurrentUser() == null){
                    finish();
                }
                break;
            case CONTACT_ACITVITY:
                if(resultCode == Activity.RESULT_OK) {
                    Intent intent = new Intent(this, ChatMessagesActivity.class);
                    Bundle b = new Bundle();
                    b.putString("chatId", data.getDataString());
                    intent.putExtras(b);
                    this.startActivity(intent);
                }
                break;
        }
    }

    public void changeViewPagerItem(int position, Fragment fragment){
        this.mainViewPagerAdapter.getFragments().set(1, fragment);
        this.mainViewPagerAdapter.notifyDataSetChanged();
        this.mainViewPagerAdapter.instantiateItem(this.viewPager, position);
    }

}
