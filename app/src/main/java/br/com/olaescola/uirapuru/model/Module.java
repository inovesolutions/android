package br.com.olaescola.uirapuru.model;

import android.graphics.Color;
import android.support.annotation.NonNull;

import java.util.List;

import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;

/**
 * Created by developer on 22/08/17.
 */

public class Module implements Comparable<Module>{
   
   private String name;
   private String moduleId;
   private String icon;
   private boolean empty;
   private RGB iconColor;
   private RGB iconBackground;
   private Version version;
   
   public String getName() {
      return name;
   }
   
   public void setName(String name) {
      this.name = name;
   }
   
   public String getModuleId() {
      return moduleId;
   }
   
   public void setModuleId(String moduleId) {
      this.moduleId = moduleId;
   }
   
   public String getIcon() {
      return icon;
   }
   
   public void setIcon(String icon) {
      this.icon = icon;
   }
   
   public List<Message> getMessages(){
      return FirebaseDataSource.getInstance().getMessages(moduleId);
   }
   
   public boolean isEmpty() {
      return empty;
   }
   
   public void setEmpty(boolean empty) {
      this.empty = empty;
   }
   
   public RGB getIconColor() {
      return iconColor;
   }
   
   public void setIconColor(RGB iconColor) {
      this.iconColor = iconColor;
   }
   
   public RGB getIconBackground() {
      return iconBackground;
   }
   
   public void setIconBackground(RGB iconBackground) {
      this.iconBackground = iconBackground;
   }
   
   public Version getVersion() {
      return version;
   }
   
   public void setVersion(Version version) {
      this.version = version;
   }
   
   public int getBadge(){
      int badge = 0;
      List<Message> messages = getMessages();
      if(messages != null){
         for(Message message: messages){
            if(!message.isRead()) badge++;
         }
      }
      return badge;
   }
   
   @Override
   public int compareTo(@NonNull Module other) {
      if(other == null || other.moduleId == null) return 0;
      return other.moduleId.compareTo(this.moduleId);
   }
   
}
