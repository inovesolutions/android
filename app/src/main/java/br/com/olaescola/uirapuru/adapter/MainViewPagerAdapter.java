package br.com.olaescola.uirapuru.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.olaescola.uirapuru.fragment.InboxFragment;
import br.com.olaescola.uirapuru.fragment.MessagesFragment;
import br.com.olaescola.uirapuru.fragment.ChatsFragment;
import br.com.olaescola.uirapuru.fragment.OptionsFragment;

public class MainViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private Fragment currentFragment;
    private final String layoutMessages;

    public MainViewPagerAdapter(FragmentManager fm, String layoutMessages) {
        super(fm);
        this.layoutMessages = layoutMessages;
        fragments.clear();
        fragments.add(new ChatsFragment());
        switch (this.layoutMessages){
            case "inbox":
                fragments.add(new InboxFragment());
                break;
            case "modules":
                fragments.add(new MessagesFragment());
                break;
        }
        fragments.add(new OptionsFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            currentFragment = ((Fragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment(){
        return currentFragment;
    }

    public ArrayList<Fragment> getFragments() {
        return fragments;
    }
}
