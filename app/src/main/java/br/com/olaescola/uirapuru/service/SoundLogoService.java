package br.com.olaescola.uirapuru.service;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import java.io.IOException;

import br.com.olaescola.uirapuru.R;

public class SoundLogoService extends IntentService {
   
   public SoundLogoService() {
      super("SoundLogoService");
   }
   
   @Nullable
   @Override
   public IBinder onBind(Intent intent) {
      return null;
   }
   
   @Override
   protected void onHandleIntent(@Nullable Intent intent) {

      if( PreferenceManager
              .getDefaultSharedPreferences(this.getApplicationContext())
              .getBoolean("switchSoundLogo", true)) {
         MediaPlayer mp = MediaPlayer.create(this.getApplicationContext(), R.raw.entrada);
         mp.start();
         mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
               mp.release();
            }
         });
      }
      
   }

}
