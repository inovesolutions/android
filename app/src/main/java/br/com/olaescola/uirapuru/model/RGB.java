package br.com.olaescola.uirapuru.model;

import android.graphics.Color;

/**
 * Created by developer on 31/08/17.
 */

public class RGB{
   
   private int r;
   private int g;
   private int b;
   
   public int getR() {
      return r;
   }
   
   public void setR(int r) {
      this.r = r;
   }
   
   public int getG() {
      return g;
   }
   
   public void setG(int g) {
      this.g = g;
   }
   
   public int getB() {
      return b;
   }
   
   public void setB(int b) {
      this.b = b;
   }
   
   public int getValue(){
      return Color.argb(255, r, g, b);
   }
}
