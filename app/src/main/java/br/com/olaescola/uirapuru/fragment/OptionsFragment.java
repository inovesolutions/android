package br.com.olaescola.uirapuru.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.terrakok.phonematter.PhoneFormat;

import java.util.ArrayList;
import java.util.List;

import br.com.olaescola.uirapuru.MainActivity;
import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.adapter.InboxAdapter;
import br.com.olaescola.uirapuru.adapter.RelationAdapter;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.helper.FontManager;
import br.com.olaescola.uirapuru.listener.RecyclerItemClickListener;
import br.com.olaescola.uirapuru.model.Message;
import br.com.olaescola.uirapuru.model.Profile;
import br.com.olaescola.uirapuru.model.Relation;

/**
 * A simple {@link Fragment} subclass.
 */
public class OptionsFragment extends Fragment implements FirebaseDataSource.OnProfileChangedListener,
FirebaseDataSource.OnRelationsChangedListener{


    private SharedPreferences preferences;
    private TextView txtName;
    private TextView txtEmail;
    private TextView txtPhone;
    private ImageView imgProfile;
    private RecyclerView recyclerRelations;
    private RelationAdapter relationAdapter;
    private LinearLayoutManager layout;
    
    public OptionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        
        View view = inflater.inflate(R.layout.fragment_options, container, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        
        recyclerRelations = (RecyclerView) view.findViewById(R.id.recycler);
        layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerRelations.setLayoutManager(layout);
    
        txtName = (TextView) view.findViewById(R.id.txtName);
        txtName.setText("");
        Typeface font = FontManager.getTypeface(this.getContext(), FontManager.WORK_SANS_BOLD);
        FontManager.markAsIconContainer(txtName, font);
        txtEmail = (TextView) view.findViewById(R.id.txtEmail);
        txtEmail.setText("");
        txtPhone = (TextView) view.findViewById(R.id.txtPhone);
        txtPhone.setText("");
        imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
        
        FirebaseDataSource.getInstance().setOnProfileChangedListener(this);
        FirebaseDataSource.getInstance().setOnRelationsChangedListener(this);
        relationAdapter = new RelationAdapter(new ArrayList<Relation>(), this.getActivity());
        relationAdapter.setRelations(FirebaseDataSource.getInstance().getRelations());
        recyclerRelations.setAdapter(relationAdapter);

        SwitchCompat switchSoundLogo = (SwitchCompat) view.findViewById(R.id.switchSoundLogo);
        switchSoundLogo.setChecked(preferences.getBoolean("switchSoundLogo", true));

        switchSoundLogo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.edit().putBoolean("switchSoundLogo", isChecked).apply();
            }
        });

        SwitchCompat switchSoundMessage = (SwitchCompat) view.findViewById(R.id.switchSoundMessage);
        switchSoundMessage.setChecked(preferences.getBoolean("switchSoundMessage", true));

        switchSoundMessage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 preferences.edit().putBoolean("switchSoundMessage", isChecked).apply();
            }
        });

        loadProfile(FirebaseDataSource.getInstance().getUser().getProfile());
        
        return view;
    }
    
    private void setImageAvatar(Profile profile, ImageView imageView) {
        if(profile.getAvatar() != null && !profile.getAvatar().isEmpty()) {

            try {
                Glide.with(this)
                    .using(new FirebaseImageLoader())
                        .load(FirebaseStorage
                                .getInstance()
                                .getReferenceFromUrl(getActivity().getString(R.string.firebase_storage_avatars))
                                .child(profile.getProfileId())
                                .child(profile.getAvatar()))
                    .fallback(R.drawable.placeholder_user)
                    .into(imageView);
                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    
    @Override
    public void onChanged(Profile profile) {
        loadProfile(profile);
    }
    
    public void loadProfile(Profile profile) {
        if(profile != null) {
            txtName.setText(profile.getDisplayName());
            txtEmail.setText(profile.getEmail());
            PhoneFormat phoneFormat = new PhoneFormat("br", getContext());
            txtPhone.setText("55" + phoneFormat.format(profile.getMobilePhone()));
            setImageAvatar(profile, imgProfile);
            relationAdapter.setRelations(FirebaseDataSource.getInstance().getRelations());
        }
    }
    
    
    @Override
    public void onChanged(Relation relation) {
        relationAdapter.setRelations(FirebaseDataSource.getInstance().getRelations());
    }
    
    @Override
    public void onAdded(Relation relation) {
        relationAdapter.setRelations(FirebaseDataSource.getInstance().getRelations());
    }
    
    @Override
    public void onRemoved(Relation relation) {
        relationAdapter.setRelations(FirebaseDataSource.getInstance().getRelations());
    }
}
