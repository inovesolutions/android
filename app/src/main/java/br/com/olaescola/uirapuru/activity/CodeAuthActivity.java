package br.com.olaescola.uirapuru.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.olaescola.uirapuru.R;

public class CodeAuthActivity extends AppCompatActivity {

   private boolean inProgress;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_code_auth);
      getSupportActionBar().setTitle("");
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      
      final String credentials = getIntent().getStringExtra("credentials");
   
      final BootstrapEditText codeInput = (BootstrapEditText) findViewById(R.id.codeAuth);
      codeInput.setRawInputType(Configuration.KEYBOARD_12KEY);
      final TextView messageLog = (TextView) findViewById(R.id.txtMessageLog);

      codeInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {
         @Override
         public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId != KeyEvent.ACTION_DOWN || inProgress) return false;
            inProgress = true;
            if(codeInput.getText().length() == 6){
               View view = CodeAuthActivity.this.getCurrentFocus();
               if (view != null) {
                  InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                  imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
               }

               try {

                  messageLog.setText(R.string.MESSAGE_SMS_VERIFYING_PHONE);

                  RequestQueue requestQueue = Volley.newRequestQueue(CodeAuthActivity.this);
                  Map<String, String> params = new HashMap<>();
                  params.put("code", codeInput.getText().toString());
                  params.put("credentials", credentials);

                  JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                 getString(R.string.cloudUrl) + "/verifyWithCredentials",
                 new JSONObject(params),
                 new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                       try {
                          getSharedPreferences("Token", MODE_PRIVATE).edit()
                                .putString("value", response.getString("token"));
                          FirebaseAuth.getInstance().signInWithCustomToken(response.getString("token"))
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                               @Override
                               public void onSuccess(AuthResult authResult) {
                                  
                                  FirebaseDatabase.getInstance().getReference().child("users")
                                          .child(authResult.getUser().getUid())
                                          .child("pushId")
                                          .setValue(FirebaseInstanceId.getInstance().getToken());
                                  FirebaseDatabase.getInstance().getReference().child("users")
                                          .child(authResult.getUser().getUid()).child("profile")
                                          .child("pushId")
                                          .setValue(FirebaseInstanceId.getInstance().getToken());
                                  messageLog.setText(R.string.MESSAGE_WAITING_FOR_CODE);
                                  setResult(RESULT_OK);
                                  inProgress = false;
                                  finish();
                               }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                               @Override
                               public void onFailure(@NonNull Exception e) {
                                  messageLog.setText(R.string.MESSAGE_WAITING_FOR_CODE);
                                  inProgress = false;
                                  Toast.makeText(CodeAuthActivity.this, "Problemas de autenticação",
                                          Toast.LENGTH_LONG).show();
                               }
                            });
                       } catch (JSONException e) {
                          e.printStackTrace();
                       }
                    }
                 },
                 new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       try {
                          if(error.networkResponse != null) {
                             JSONObject response = new JSONObject(new String(error.networkResponse.data));
                             switch (response.getString("code")) {
                                case "401.10":
                                   Toast.makeText(CodeAuthActivity.this,
                                           "As informações de Credencial não foram enviadas ao servidor de autenticação",
                                           Toast.LENGTH_LONG).show();
                                   break;
                                case "401.20":
                                   Toast.makeText(CodeAuthActivity.this,
                                           "O código informado expirou",
                                           Toast.LENGTH_LONG).show();
                                   break;
                                case "401.30":
                                   Toast.makeText(CodeAuthActivity.this,
                                           "Código Inválido",
                                           Toast.LENGTH_LONG).show();
                                   break;
                                case "401.40":
                                   Toast.makeText(CodeAuthActivity.this,
                                           "Este código não está associado ao celular informado",
                                           Toast.LENGTH_LONG).show();
                                   break;
                                case "401.50":
                                   Toast.makeText(CodeAuthActivity.this,
                                           "Código Inválido",
                                           Toast.LENGTH_LONG).show();
                                   break;
                                case "400.10":
                                   Toast.makeText(CodeAuthActivity.this,
                                           "Falha nos servidores de autenticação",
                                           Toast.LENGTH_LONG).show();
                                   break;
                             }
                          }
                          else{
                             Toast.makeText(CodeAuthActivity.this,
                                     "Tempo de espera esgotado, não foi possível completar a operação",
                                     Toast.LENGTH_LONG).show();
                          }

                          messageLog.setText(R.string.MESSAGE_WAITING_FOR_CODE);
                          inProgress = false;

                       } catch (JSONException e) {
                          e.printStackTrace();
                       }
                    }
                 });

                  requestQueue.add(request);

               } catch (Exception ex) {
                  ex.printStackTrace();
               }
            }
            return false;
         }
      });
   }
   
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      this.finish();
      return super.onOptionsItemSelected(item);
   }
}
