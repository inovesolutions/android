package br.com.olaescola.uirapuru.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by developer on 18/08/17.
 */

public class User {
   
   private String userId;
   private String pushId;
   private List<Chat> chats;
   private List<Contact> contacts;
   private Profile profile;
   private List<String> tags;
   private List<Message> messages;
   private List<Relation> relations;
   
   public String getUserId() {
      return userId;
   }
   
   public void setUserId(String userId) {
      this.userId = userId;
   }
   
   public String getPushId() {
      return pushId;
   }
   
   public void setPushId(String pushId) {
      this.pushId = pushId;
   }
   
   public List<Chat> getChats() {
      if(chats == null) chats = new ArrayList<>();
      return chats;
   }
   
   public void setChats(List<Chat> chats) {
      this.chats = chats;
   }
   
   public List<Contact> getContacts() {
      if(contacts == null) contacts = new ArrayList<>();
      return contacts;
   }
   
   public void setContacts(List<Contact> contacts) {
      this.contacts = contacts;
   }
   
   public Profile getProfile() {
      if(profile == null) profile = new Profile();
      return profile;
   }
   
   public void setProfile(Profile profile) {
      this.profile = profile;
   }
   
   public List<String> getTags() {
      if(tags == null) tags = new ArrayList<>();
      return tags;
   }
   
   public void setTags(List<String> tags) {
      this.tags = tags;
   }
   
   public List<Message> getMessages() {
      if(messages == null) messages = new ArrayList<>();
      return messages;
   }
   
   public void setMessages(List<Message> messages) {
      this.messages = messages;
   }
   
   public List<Relation> getRelations() {
      if(relations == null) relations = new ArrayList<>();
      return relations;
   }
   
   public void setRelations(List<Relation> relations) {
      this.relations = relations;
   }
   
}
