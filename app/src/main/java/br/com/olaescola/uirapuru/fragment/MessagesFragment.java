package br.com.olaescola.uirapuru.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

import java.util.ArrayList;
import java.util.List;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.adapter.MessagesAdapter;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.model.Module;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment implements FirebaseDataSource.OnModulesChangeListener{


    private RecyclerView recycler;
    private FirebaseDataSource dataSource;
    private LinearLayoutManager layout;
    private MessagesAdapter adapter;
    private AHBottomNavigation bottomNavigation;

    public MessagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        dataSource = FirebaseDataSource.getInstance();
        dataSource.setOnModulesChangeListener(this);
        layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler.setLayoutManager(layout);
        adapter = new MessagesAdapter(new ArrayList<Module>(), this.getActivity(), layout);
        recycler.setAdapter(adapter);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.channels_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.bottom_navigation);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

        recycler.setLayoutManager(layout);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(dataSource.getModules() != null) {
            adapter.setModules(dataSource.getModules());
            adapter.notifyDataSetChanged();
        }
    }
    
    @Override
    public void onChanged(List<Module> modules) {
        if(adapter != null) {
            if (modules!= null) {
                adapter.setModules(modules);
            
                int qtdeNotRead = 0;
                for(Module module : modules) {
                    qtdeNotRead += module.getBadge();
                }
                if (qtdeNotRead > 0) {
                    bottomNavigation.setNotification(String.valueOf(qtdeNotRead), 1);
                } else {
                    bottomNavigation.setNotification("", 1);
                }
            }
        }
    }
}
