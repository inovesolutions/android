package br.com.olaescola.uirapuru.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;

import java.io.File;
import java.util.List;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.helper.FontManager;
import br.com.olaescola.uirapuru.helper.ImageHelper;
import br.com.olaescola.uirapuru.model.Contact;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContactAdapter extends RecyclerView.Adapter {
   
   private List<Contact> contacts;
   private Context context;
   
   public ContactAdapter(List<Contact> contacts, Context context) {
      this.contacts = contacts;
      this.context = context;
   }
   
   @Override
   public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View itemView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.contact_item, parent, false);
      ViewHolder holder = new ViewHolder(itemView, context);
      itemView.setOnClickListener(holder);
      return holder;
   }
   
   @Override
   public int getItemCount() {
      return this.contacts.size();
   }
   
   @Override
   public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

      final ViewHolder viewHolder = (ViewHolder) holder;
      final Contact contact = this.contacts.get(position);
      viewHolder.contact = contact;
      viewHolder.name.setText(contact.getDisplayName());

      if (contact.getAvatar() != null) {

         final File avatar = ImageHelper.getDownloadFilePath("Avatars", contact.getAvatar());
         if (avatar.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(avatar.getPath());
            if (bitmap != null) {
               viewHolder.avatar.setImageBitmap(bitmap);
            }
         } else {
            try {
               FirebaseStorage
                       .getInstance().getReferenceFromUrl(context
                       .getString(R.string.firebase_storage_avatars)
                       .concat("/").concat(contact.getProfileId())
                       .concat("/").concat(contact.getAvatar()))
                       .getFile(avatar)
                       .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                          @Override
                          public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                             Bitmap bitmap = BitmapFactory.decodeFile(avatar.getPath());
                             viewHolder.avatar.setImageBitmap(bitmap);
                          }
                       });
            } catch (Exception ex) {
               System.out.println(ex.getMessage());
               Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                       R.drawable.placeholder_user);
               viewHolder.avatar.setImageBitmap(bitmap);
            }
         }
      }
      else {
         Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                 R.drawable.placeholder_user);
         viewHolder.avatar.setImageBitmap(bitmap);
      }
   }
   
   static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
      
      private Contact contact;
      private TextView name;
      private CircleImageView avatar;
      private Context context;
      
      public ViewHolder(View itemView, Context context) {
         super(itemView);
         name = (TextView) itemView.findViewById(R.id.txtName);
         Typeface font = FontManager.getTypeface(context, FontManager.WORK_SANS_BOLD);
         FontManager.markAsIconContainer(name, font);
         avatar = (CircleImageView) itemView.findViewById(R.id.imgAvatar);
         this.context = context;
      }
      
      @Override
      public void onClick(View v) {
         Intent data = new Intent();
         data.setData(Uri.parse(contact.getProfileId()));
         ((AppCompatActivity) context).setResult(Activity.RESULT_OK, data);
         ((AppCompatActivity) context).finish();
      }
   }
}
