package br.com.olaescola.uirapuru.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by rafae on 23/06/2017.
 */

public class ImageHelper {

    public static final String COLEGIO_UIRAPURU = "Colegio Uirapuru/Recebidos/";
    public static final String COLEGIO_UIRAPURU_UPLOAD = "Colegio Uirapuru/Enviados";

    public static File getDownloadFilePath(String type, String fileName) {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), COLEGIO_UIRAPURU + type);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        return new File(mediaStorageDir.getPath() + File.separator + fileName);
    }

    public static Bitmap scaleBitmap(Bitmap bitmap) {

        int maxWidth = 1920;
        int maxHeight = 1920;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width > height && width > maxWidth) {
            int ratio = width / maxWidth;
            width = maxWidth;
            height = height / ratio;
        } else if (height > width && height > maxHeight) {
            int ratio = height / maxHeight;
            height = maxHeight;
            width = width / ratio;
        }

        bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        return bitmap;
    }

    public static File getUploadFilePath(String fileName) {

        File storageData = new File(Environment.getExternalStorageDirectory(), COLEGIO_UIRAPURU_UPLOAD);

        if (!storageData.exists()) {
            if (!storageData.mkdirs()) {
                return null;
            }
        }

        return new File(storageData.getPath() + File.separator + fileName);
    }

    public static File getUploadFilePath(File source, String fileName) {

        FileChannel inChannel = null;
        FileChannel outChannel = null;
        File destination = ImageHelper.getUploadFilePath(fileName);

        try {
            inChannel = new FileInputStream(source).getChannel();
            outChannel = new FileOutputStream(destination).getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inChannel != null) inChannel.close();
                if (outChannel != null) outChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return destination;
    }
    
    public static class BlurTransformation extends BitmapTransformation {

        private RenderScript rs;

        public BlurTransformation(Context context) {
            super( context );

            rs = RenderScript.create( context );
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            Bitmap blurredBitmap = toTransform.copy( Bitmap.Config.ARGB_8888, true );

            Allocation input = Allocation.createFromBitmap(
                    rs,
                    blurredBitmap,
                    Allocation.MipmapControl.MIPMAP_FULL,
                    Allocation.USAGE_SHARED
            );

            Allocation output = Allocation.createTyped(rs, input.getType());
            ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            script.setInput(input);
            script.setRadius(10);
            script.forEach(output);
            output.copyTo(blurredBitmap);
            toTransform.recycle();

            return blurredBitmap;
        }

        @Override
        public String getId() {
            return "blur";
        }
    }
}
