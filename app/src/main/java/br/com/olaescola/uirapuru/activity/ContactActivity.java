package br.com.olaescola.uirapuru.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.easyandroidanimations.library.FadeInAnimation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.adapter.ContactAdapter;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.model.Contact;
import br.com.olaescola.uirapuru.protocol.SearchInterface;

public class ContactActivity extends AppCompatActivity
        implements FirebaseDataSource.OnContactsChangedListener, SearchInterface {

    private RecyclerView recycler;
    private ContactAdapter adapter;
    private boolean inProgress;
    private ArrayList<Contact> contacts;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Contatos");

        recycler = (RecyclerView) findViewById(R.id.recycler);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        recycler.setLayoutManager(layout);
        
        contacts = new ArrayList<>(FirebaseDataSource.getInstance().getContacts().size());
        contacts.addAll(FirebaseDataSource.getInstance().getContacts());
   
        adapter = new ContactAdapter(contacts, this);
        recycler.setAdapter(adapter);

        FirebaseDataSource.getInstance().setOnContactsChangedListener(this);
    
        final BootstrapEditText inputSearch = (BootstrapEditText) findViewById(R.id.search);
    
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        
            }
    
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (inProgress) return;
                inProgress = true;
                contacts.clear();
                String term = s.toString();
                if("".equals(term.replaceAll("\\s+",""))){
                    contacts.addAll(FirebaseDataSource.getInstance().getContacts());
                }
                else{
                    for(Contact c : FirebaseDataSource.getInstance().getContacts()){
                        if(c.getDisplayName().toLowerCase().contains(term.toLowerCase())
                              || c.getEmail() != null && c.getEmail().toLowerCase().contains(term.toLowerCase())){
                            contacts.add(c);
                        }
                    }
                }
    
                if(adapter != null){
                    adapter.notifyDataSetChanged();
                }
    
                inProgress = false;
            }
    
            @Override
            public void afterTextChanged(Editable s) {
        
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new FadeInAnimation(recycler).setDuration(550).animate();
    }

    @Override
    public void onChanged(Contact contact) {
        if(adapter != null){
            adapter.notifyDataSetChanged();
        }
    }
    
    @Override
    public void onAdded(Contact contact) {
        contacts.add(contact);
        if(adapter != null){
            adapter.notifyDataSetChanged();
        }
    }
    
    @Override
    public void onRemoved(Contact contact) {
        
    }
    
    @Override
    public void onInputChange(String term) {
        
    }
}
