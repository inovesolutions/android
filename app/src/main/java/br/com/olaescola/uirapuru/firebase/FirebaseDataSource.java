package br.com.olaescola.uirapuru.firebase;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.model.Chat;
import br.com.olaescola.uirapuru.model.Contact;
import br.com.olaescola.uirapuru.model.Message;
import br.com.olaescola.uirapuru.model.Module;
import br.com.olaescola.uirapuru.model.Presence;
import br.com.olaescola.uirapuru.model.Profile;
import br.com.olaescola.uirapuru.model.Relation;
import br.com.olaescola.uirapuru.model.User;

public class FirebaseDataSource {

   private static final FirebaseDataSource own = new FirebaseDataSource();

   private Context context;
   private boolean serviceStarted = false;
   private User user;
   private List<Module> modules;
   private Map<String, Presence> presence;

   private OnChatsChangedListener onChatsChangedListener;
   private OnContactsChangedListener onContactsChangedListener;
   private OnMessagesChangedListener onMessagesChangedListener;
   private OnProfileChangedListener onProfileChangedListener;
   private OnTagsChangedListener onTagsChangedListener;
   private OnModulesChangeListener onModulesChangeListener;
   private OnRelationsChangedListener onRelationsChangedListener;
   private OnNewChatMessageListener onNewChatMessageListener;

   public void setOnChatsChangedListener(OnChatsChangedListener onChatsChangedListener) {
      this.onChatsChangedListener = onChatsChangedListener;
   }

   public void setOnContactsChangedListener(OnContactsChangedListener onContactsChangedListener) {
      this.onContactsChangedListener = onContactsChangedListener;
   }

   public void setOnMessagesChangedListener(OnMessagesChangedListener onMessagesChangedListener) {
      this.onMessagesChangedListener = onMessagesChangedListener;
   }

   public void setOnProfileChangedListener(OnProfileChangedListener onProfileChangedListener) {
      this.onProfileChangedListener = onProfileChangedListener;
   }

   public void setOnTagsChangedListener(OnTagsChangedListener onTagsChangedListener) {
      this.onTagsChangedListener = onTagsChangedListener;
   }

   public void setOnModulesChangeListener(OnModulesChangeListener onModulesChangeListener) {
      this.onModulesChangeListener = onModulesChangeListener;
   }

   public void setOnRelationsChangedListener(OnRelationsChangedListener onRelationsChangedListener) {
      this.onRelationsChangedListener = onRelationsChangedListener;
   }

   public void setOnNewChatMessageListener(OnNewChatMessageListener onNewChatMessageListener) {
      this.onNewChatMessageListener = onNewChatMessageListener;
   }

   private FirebaseDataSource(){
      user = new User();
      modules = new ArrayList<>();
      presence = new HashMap<>();
   }

   public static FirebaseDataSource getInstance(FirebaseUser firebaseUser) {
      own.user.setUserId(firebaseUser.getUid());
      return own;
   }

   public static FirebaseDataSource getInstance() {
      return own;
   }

   public boolean connect(Context context) {

      if (serviceStarted) return true;
      this.context = context;
      if (this.user.getUserId() != null) {
         syncUser();
         serviceStarted = true;
         return true;
      }
      else{
         return false;
      }
   }

   private void syncUser(){
      DatabaseReference usersReference = FirebaseDatabase.getInstance()
              .getReference().child("users").child(user.getUserId());
      syncContacts(usersReference.child("contacts"));
      syncChats(usersReference.child("chats"));
      syncMessages(usersReference.child("messages"));
      syncProfile(usersReference.child("profile"));
      syncTags(usersReference.child("tags"));
      syncRelations(usersReference.child("relations"));
      syncModules();

      final DatabaseReference presenceRef = FirebaseDatabase
              .getInstance().getReference().child("presence").child(user.getUserId());


      FirebaseDatabase.getInstance().getReference(".info/connected")
              .addValueEventListener(new ValueEventListener() {
                 @Override
                 public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                       presenceRef.child("connected").setValue(true);
                       presenceRef.child("name").setValue(user.getProfile().getDisplayName());
                       presenceRef.child("timestamp").setValue(ServerValue.TIMESTAMP);
                    }
                 }

                 @Override
                 public void onCancelled(DatabaseError databaseError) {

                 }
              });


      presenceRef.child("connected").onDisconnect().setValue(false);
      presenceRef.child("timestamp").onDisconnect().setValue(ServerValue.TIMESTAMP);

   }

   private void syncContacts(DatabaseReference contactReference){
      this.user.getContacts().clear();
      contactReference.addChildEventListener(new ContactSynchronizer());
   }

   private void syncChats(DatabaseReference chatsReference){
      user.getChats().clear();
      chatsReference.addChildEventListener(new ChatsSynchronizer());
   }

   private void syncMessages(DatabaseReference messagesReference){
      user.getMessages().clear();
      messagesReference.addChildEventListener(new MessagesSynchronizer());
   }

   private void syncProfile(DatabaseReference profileReference){
      profileReference.addValueEventListener(new ProfileSynchronizer());
   }

   private void syncTags(DatabaseReference tagsReference){
      user.getTags().clear();
      tagsReference.addChildEventListener(new TagsSynchronizer());
   }

   private void syncModules(){
      FirebaseDatabase.getInstance().getReference()
            .child("modules").addValueEventListener(new ValueEventListener() {
         @Override
         public void onDataChange(DataSnapshot dataSnapshot) {
            if(dataSnapshot.exists()){
               modules.clear();
               Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
               while(iterator.hasNext()){
                  DataSnapshot nextModuleDataSnapshot = iterator.next();
                  Module module = nextModuleDataSnapshot.getValue(Module.class);
                  module.setModuleId(nextModuleDataSnapshot.getKey());
                  modules.add(module);
               }
            }

            if(onModulesChangeListener != null) onModulesChangeListener.onChanged(modules);
         }

         @Override
         public void onCancelled(DatabaseError databaseError) {

         }
      });
   }

   private void syncRelations(DatabaseReference relationsReference){
      this.user.getRelations().clear();
      relationsReference.getRef().addChildEventListener(new RelationSynchronizer());
   }

   private void addContact(DataSnapshot contactDataSnapshot) {
      Contact contact = contactDataSnapshot.getValue(Contact.class);
      contact.setProfileId(contactDataSnapshot.getKey());
      this.user.getContacts().add(contact);
      if(onContactsChangedListener != null) onContactsChangedListener.onAdded(contact);
   }

   private void addRelation(DataSnapshot relationDataSnapshot) {
      Relation relation = relationDataSnapshot.getValue(Relation.class);
      relation.getProfile().setProfileId(relationDataSnapshot.getKey());
      this.user.getRelations().add(relation);
      if(onRelationsChangedListener != null) onRelationsChangedListener.onAdded(relation);
   }

   private void addChat(DataSnapshot nextChatSnapshot) {
      final Chat chat = new Chat();
      chat.fill(nextChatSnapshot);
      user.getChats().add(chat);
      if(onChatsChangedListener != null) {
         onChatsChangedListener.onAdded(chat);
      }

      FirebaseDatabase.getInstance().getReference().child("presence").child(chat.getContactId())
            .addValueEventListener(new ValueEventListener() {
               @Override
               public void onDataChange(DataSnapshot dataSnapshot) {
                  if(dataSnapshot.exists()){
                     presence.put(chat.getContactId(), dataSnapshot.getValue(Presence.class));
                     if(onChatsChangedListener != null) {
                        onChatsChangedListener.onAdded(chat);
                     }
                  }
   
                  new android.os.Handler().postDelayed(
                  new Runnable() {
                     public void run() {
                        FirebaseDatabase.getInstance()
                        .getReference()
                        .child("users").child(FirebaseDataSource.getInstance().getUser().getUserId())
                        .child("chats").child(chat.getContactId()).child("writing").setValue(false);
                     }
                  }, 3000);
               }

               @Override
               public void onCancelled(DatabaseError databaseError) {

               }
            });


   }

   public Presence findPresence(String contactId){
      return presence.get(contactId);
   }

   private void addMessage(DataSnapshot messageDataSnapshot){

      Message message = messageDataSnapshot.getValue(Message.class);
      message.setMessageId(messageDataSnapshot.getKey());

      if(message.getContent().getHeader().getRelations() != null){
         Iterator<Map.Entry<String, Relation>> iterator = message
                 .getContent().getHeader().getRelations().entrySet().iterator();
         while(iterator.hasNext()){
            Map.Entry<String, Relation> next = iterator.next();
            Relation relation = next.getValue();
            relation.getProfile().setProfileId(next.getKey());
            message.getContent().getHeader().getRelations().put(next.getKey(), relation);
         }
      }

      if(!message.isReceived()) {
         message.setReceived(true);
         FirebaseDatabase.getInstance().getReference()
               .child("users").child(user.getUserId()).child("messages")
               .child(message.getMessageId()).child("received").setValue(true);
      }

      user.getMessages().add(message);
      if(onMessagesChangedListener != null) {
         onMessagesChangedListener.onAdded(message);
      }
   }

   
   private void addTag(DataSnapshot tagDataSnapshot){
      String tag = tagDataSnapshot.getKey();
      user.getTags().add(tag);
      if(onTagsChangedListener != null) onTagsChangedListener.onAdded(tag);
   }
   
   public void setChatMessageAsRead(Chat.Message message, String chatId) {
   
      if(currentUserIsSender(message) || message.isRead()) return;
      
      message.setRead(true);
      
      FirebaseDatabase.getInstance().getReference()
            .child("users").child(user.getUserId()).child("chats")
            .child(chatId).child("messages").child(message.getMessageId())
            .child("read").setValue(true);
      
      FirebaseDatabase.getInstance().getReference()
            .child("users").child(chatId).child("chats")
            .child(user.getUserId()).child("messages").child(message.getMessageId())
            .child("read").setValue(true);
   }

   public boolean currentUserIsSender(Chat.Message message) {
      if(message.getSenderId() == null) return false;
      return message.getSenderId().equals(FirebaseDataSource.getInstance().getUser().getUserId());
   }

   public void removeChatMessages(List<Chat.Message> selectedMessages, String contactId) {
      HashMap messagesToUpdate = new HashMap();
      for(Chat.Message message : selectedMessages){
         message.setDeleted(true);
         messagesToUpdate.put("/users/" + user.getUserId()
                 + "/chats/" + contactId + "/messages/" + message.getMessageId() + "/deleted", true);
         message.setMessageId(null);
      }
      
      FirebaseDatabase.getInstance().getReference()
            .updateChildren(messagesToUpdate)
            .addOnFailureListener(new OnFailureListener() {
               @Override
               public void onFailure(@NonNull Exception e) {
                  System.out.println(e.getMessage());
               }
            });

      selectedMessages.clear();
   }
   
   public DatabaseReference saveChatMessage(final Chat.Message message, final String contactId) {

      message.setSenderId(user.getUserId());

      final DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
            .child("users").child(user.getUserId())
            .child("chats").child(contactId).child("messages").push();

      message.setMessageId(reference.getKey());

      Profile profile;
      Contact contact = findContact(contactId);

      if(contact != null){ profile = contact; }
      else{
         profile = findChat(contactId).getProfile();
      }

      Chat chat = this.findChat(contactId);
      if(chat != null) {
         chat.getMessages().add(message);
         if(onChatsChangedListener != null) { onChatsChangedListener.onChanged(chat); }
      }

      Map<String, Object> update = new HashMap<>();

      update.put("/users/" + user.getUserId() + "/chats/" + contactId + "/profile", profile);
      update.put("/users/" + user.getUserId() + "/chats/" + contactId + "/lastChange", ServerValue.TIMESTAMP);
      update.put("/users/" + user.getUserId() + "/chats/" + contactId + "/messages/" + reference.getKey(), message.toMap());
      update.put("/users/" + contactId + "/chats/" + user.getUserId() + "/profile", user.getProfile());
      update.put("/users/" + contactId + "/chats/" + user.getUserId() + "/lastChange", ServerValue.TIMESTAMP);
      update.put("/users/" + contactId + "/chats/" + user.getUserId() + "/messages/" + reference.getKey(), message.toMap());

      FirebaseDatabase.getInstance().getReference()
            .updateChildren(update)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
               @Override
               public void onSuccess(Void aVoid) {

               }
            });

      return reference;
   }

   public void uploadFileChatMessage(final String contactId, final String messageId, final Chat.Message message) {
      final File file = new File(message.getPath());
      Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

      if(bitmap == null) return;

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);
      byte[] data = baos.toByteArray();

      UploadTask uploadTask = FirebaseStorage.getInstance().getReference()
        .child("users")
        .child(contactId)
        .child("chats")
        .child(FirebaseDataSource.getInstance().getUser().getUserId())
        .child(messageId)
        .child(message.getContent()).putBytes(data);

      uploadTask.addOnFailureListener(new OnFailureListener() {
         @Override
         public void onFailure(@NonNull Exception exception) {
            System.out.println(exception.getMessage());
         }
      }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
         @Override
         public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            Map<String, Object> update = new HashMap<>();
            update.put("/users/" + user.getUserId() + "/chats/" + contactId + "/messages/" + messageId + "/uploaded", true);
            update.put("/users/" + contactId + "/chats/" + user.getUserId() + "/messages/" + messageId + "/uploaded", true);
            FirebaseDatabase.getInstance().getReference()
                    .updateChildren(update);
         }
      });
   }
   
   public Chat findChat(String contactId){
      
      if(contactId != null){
         for(Chat chat : user.getChats()){
            if(contactId.equals(chat.getContactId())){
               return chat;
            }
         }
      }
      
      return null;
   }

   public Contact findContact(String contactId){
      if(contactId != null){
         for(Contact contact : user.getContacts()){
            if(contactId.equals(contact.getProfileId())){
               return contact;
            }
         }
      }

      return null;
   }
   
   public List<Chat> getChats() {
      return user.getChats();
   }
   
   public void removeChats(List<Chat> selectedChats) {
      for (Chat chat : selectedChats) {
         removeChatMessages(chat.getMessages(), chat.getContactId());
      }
      selectedChats.clear();
   }
   
   public void removeMessages(List<Message> selectedMessages) {
      
      HashMap messagesToUpdate = new HashMap();
      for(Message message : selectedMessages){
         message.setDeleted(true);
         try {
            Message clone = (Message) message.clone();
            clone.setMessageId(null);
            messagesToUpdate.put(message.getMessageId(), clone);
         } catch (CloneNotSupportedException e) {
            e.printStackTrace();
         }
      }
   
      FirebaseDatabase.getInstance().getReference()
            .child("users").child(user.getUserId())
            .child("messages").updateChildren(messagesToUpdate);
      
   }
   
   public List<Message> getMessages() {
      return user.getMessages();
   }
   
   public List<Message> getMessages(String moduleId) {
      List<Message> messages = new ArrayList<>();
      for(Message message: user.getMessages()){
         if(message.getContent().getHeader().getModuleId().equals(moduleId)){
            messages.add(message);
         }
      }
      return messages;
   }
   
   public List<Module> getModules() {
      return modules;
   }
   
   public Message findMessage(String messageId) {
      for (Message message : user.getMessages()) {
         if (message.getMessageId().equals(messageId)){
            return message;
         }
      }
      return null;
   }
   
   public void setMessageAsRead(Message message) {
      if(!message.isRead()) {
         message.setRead(true);
         FirebaseDatabase.getInstance().getReference()
               .child("users").child(user.getUserId()).child("messages")
               .child(message.getMessageId()).child("read").setValue(true);
      }
   }
   
   public List<Contact> getContacts() {
      return user.getContacts();
   }
   
   public List<Relation> getRelations() {
      return user.getRelations();
   }
   
   private class ContactSynchronizer implements ChildEventListener{
   
      @Override
      public void onChildAdded(DataSnapshot contactDataSnapshot, String previousChildName) {
         addContact(contactDataSnapshot);
      }
   
      @Override
      public void onChildChanged(DataSnapshot contactDataSnapshot, String previousChildName) {
         for(Contact contact : user.getContacts()){
            if(contact.getProfileId().equals(contactDataSnapshot.getKey())){
               contact = contactDataSnapshot.getValue(Contact.class);
               contact.setProfileId(contactDataSnapshot.getKey());
               if(onContactsChangedListener != null) onContactsChangedListener.onChanged(contact);
               break;
            }
         }
      }
   
      @Override
      public void onChildRemoved(DataSnapshot contactDataSnapshot) {
         for(Contact contact: user.getContacts()){
            if(contact.getProfileId().equals(contactDataSnapshot.getKey())){
               contact = contactDataSnapshot.getValue(Contact.class);
               contact.setProfileId(contactDataSnapshot.getKey());
               if(onContactsChangedListener != null) onContactsChangedListener.onRemoved(contact);
               break;
            }
         }
      }
   
      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
      
      }
   
      @Override
      public void onCancelled(DatabaseError databaseError) {
      
      }
   }
   
   private class RelationSynchronizer implements ChildEventListener{
      
      @Override
      public void onChildAdded(DataSnapshot relationDataSnapshot, String previousChildName) {
         addRelation(relationDataSnapshot);
      }
      
      @Override
      public void onChildChanged(DataSnapshot relationDataSnapshot, String previousChildName) {
         for(Relation relation : user.getRelations()){
            if(relation.getProfile().getProfileId().equals(relationDataSnapshot.getKey())){
               relation = relationDataSnapshot.getValue(Relation.class);
               relation.getProfile().setProfileId(relationDataSnapshot.getKey());
               if(onRelationsChangedListener != null) onRelationsChangedListener.onChanged(relation);
               break;
            }
         }
      }
      
      @Override
      public void onChildRemoved(DataSnapshot relationDataSnapshot) {
         for(Relation relation : user.getRelations()){
            if(relation.getProfile().getProfileId().equals(relationDataSnapshot.getKey())){
               user.getRelations().remove(relation);
               if(onRelationsChangedListener != null) onRelationsChangedListener.onChanged(relation);
               break;
            }
         }
      }
      
      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
         
      }
      
      @Override
      public void onCancelled(DatabaseError databaseError) {
         
      }
   }
   
   private class ChatsSynchronizer implements ChildEventListener{
   
      @Override
      public void onChildAdded(DataSnapshot chatDataSnapshot, String previousChildName) {
         addChat(chatDataSnapshot);
      }
   
      @Override
      public void onChildChanged(DataSnapshot chatDataSnapshot, String previousChildName) {

         if(!chatDataSnapshot.exists()) return;
         if(!chatDataSnapshot.child("profile").exists()) return;
         if(!chatDataSnapshot.child("lastChange").exists()) return;

         for(final Chat chat: user.getChats()){

            if(chat.getContactId().equals(chatDataSnapshot.getKey())){
               chat.fill(chatDataSnapshot);
               if(onChatsChangedListener != null) {
                  onChatsChangedListener.onChanged(chat);
               }

               new android.os.Handler().postDelayed(
                       new Runnable() {
                          public void run() {
                             FirebaseDatabase.getInstance()
                            .getReference()
                            .child("users").child(FirebaseDataSource.getInstance().getUser().getUserId())
                            .child("chats").child(chat.getContactId()).child("writing").setValue(false);
                          }
                       }, 3000);
               break;
            }
         }
      }
   
      @Override
      public void onChildRemoved(DataSnapshot chatDataSnapshot) {
         for(Chat chat: user.getChats()){
            if(chat.getContactId().equals(chatDataSnapshot.getKey())){
               user.getChats().remove(chat);
               if(onChatsChangedListener != null) onChatsChangedListener.onRemoved(chat);
               break;
            }
         }
      }
   
      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
      
      }
   
      @Override
      public void onCancelled(DatabaseError databaseError) {
      
      }
   }
   
   private class MessagesSynchronizer implements ChildEventListener{
   
      @Override
      public void onChildAdded(DataSnapshot messageDataSnapshot, String s) {
         addMessage(messageDataSnapshot);
      }
   
      @Override
      public void onChildChanged(DataSnapshot messageDataSnapshot, String s) {
         
         for(Message message : user.getMessages()){
            
            if(messageDataSnapshot.getKey().equals(message.getMessageId())){
   
               Message newMessage = messageDataSnapshot.getValue(Message.class);
               newMessage.setMessageId(messageDataSnapshot.getKey());
               int index = user.getMessages().indexOf(message);
               user.getMessages().set(index, newMessage);
               if(onMessagesChangedListener != null) onMessagesChangedListener.onChanged(message);
               
               break;
               
            }
         }

      }
   
      @Override
      public void onChildRemoved(DataSnapshot messageDataSnapshot) {
         for(Message message: user.getMessages()){
            if(message.getMessageId().equals(messageDataSnapshot.getKey())){
               user.getMessages().remove(message);
               if(onMessagesChangedListener != null) onMessagesChangedListener.onRemoved(message);
               break;
            }
         }
      }
   
      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
      
      }
   
      @Override
      public void onCancelled(DatabaseError databaseError) {
      
      }
   }
   
   private class ProfileSynchronizer implements ValueEventListener{
   
      @Override
      public void onDataChange(DataSnapshot profileDataSnapshot) {
         Profile profile = profileDataSnapshot.getValue(Profile.class);
         user.setProfile(profile);
         user.getProfile().setProfileId(user.getUserId());
         if(onProfileChangedListener != null) onProfileChangedListener.onChanged(profile);
      }
   
      @Override
      public void onCancelled(DatabaseError databaseError) {
      
      }
   }
   
   private class TagsSynchronizer implements ChildEventListener{
   
      @Override
      public void onChildAdded(DataSnapshot tagDataSnapshot, String s) {
         addTag(tagDataSnapshot);
      }
   
      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String s) {
      
      }
   
      @Override
      public void onChildRemoved(DataSnapshot tagDataSnapshot) {
         for(String tag: user.getTags()){
            if(tag.equals(tagDataSnapshot.getKey())){
               user.getTags().remove(tag);
               if(onTagsChangedListener != null) onTagsChangedListener.onRemoved(tag);
               break;
            }
         }
      }
   
      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
      
      }
   
      @Override
      public void onCancelled(DatabaseError databaseError) {
      
      }
   }
   
   public interface OnChatsChangedListener{
      void onChanged(Chat chat);
      void onAdded(Chat chat);
      void onRemoved(Chat chat);
   }
   
   public interface OnContactsChangedListener{
      void onChanged(Contact contact);
      void onAdded(Contact contact);
      void onRemoved(Contact contact);
   }
   
   public interface OnMessagesChangedListener{
      void onChanged(Message message);
      void onAdded(Message message);
      void onRemoved(Message message);
   }
   
   public interface OnProfileChangedListener{
      void onChanged(Profile profile);
   }
   
   public interface OnTagsChangedListener{
      void onChanged(String tag);
      void onAdded(String tag);
      void onRemoved(String tag);
   }
   
   public interface OnModulesChangeListener{
      void onChanged(List<Module> modules);
   }
   
   public interface OnRelationsChangedListener{
      void onChanged(Relation relation);
      void onAdded(Relation relation);
      void onRemoved(Relation relation);
   }
   
   public interface OnNewChatMessageListener{
      void onNew(String chatId);
   }
   
   public List<Chat.Message> getChatMessages(String contactId){
      List messages = null;
      for(Chat chat: user.getChats()){
         if(chat.getContactId().equals(contactId)){
            messages = chat.getMessages();
            break;
         }
      }
      
      return messages;
   }
   
   public User getUser() {
      return user;
   }
}