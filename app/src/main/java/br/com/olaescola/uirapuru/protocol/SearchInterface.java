package br.com.olaescola.uirapuru.protocol;

/**
 * Created by developer on 01/11/17.
 */

public interface SearchInterface {
   public void onInputChange(String term);
}
