package br.com.olaescola.uirapuru.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar.LayoutParams;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.louvre.Gallery;
import com.andremion.louvre.home.GalleryActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import  com.easyandroidanimations.library.ScaleInAnimation;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import br.com.olaescola.uirapuru.OlaEscolaApplication;
import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.adapter.ChatMessageAdapter;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.fragment.ImageConfirmationFragment;
import br.com.olaescola.uirapuru.fragment.ImageFullScreenFragment;
import br.com.olaescola.uirapuru.helper.ChatHelper;
import br.com.olaescola.uirapuru.helper.FontManager;
import br.com.olaescola.uirapuru.helper.ImageHelper;
import br.com.olaescola.uirapuru.listener.RecyclerItemClickListener;
import br.com.olaescola.uirapuru.model.Chat;
import br.com.olaescola.uirapuru.model.Contact;
import br.com.olaescola.uirapuru.model.Message;
import br.com.olaescola.uirapuru.model.Presence;

public class ChatMessagesActivity extends AppCompatActivity
        implements RecyclerItemClickListener.OnItemClickListener,
        FirebaseDataSource.OnChatsChangedListener{

    public static String RECYCLER_SCROLL_POSITION = "chat:recycler:position";
    private static final int REQUEST_IMAGE_CAPTURE = 0x100;
    private static final int REQUEST_FILE_CHOOSER = 0x200;
    private static final int PERMISSION_CAMERA = 0X300;
    private ChatHelper chatHelper;
    private Gallery gallery;
    private List<Chat.Message> selectedMessages = new ArrayList<>();
    private ChatMessageAdapter adapter;
    private RecyclerView recycler;
    private EditText edtMessage;
    private LinearLayoutManager layoutManager;
    private TextView iconCamera;
    private ImageConfirmationFragment imageConfirmationFragment;
    private boolean hasImage = false;
    private Bitmap bitmap;
    private TextView iconSend;
    private String imageName;
    private SharedPreferences preferences;
    private File imageFile;
    private FirebaseDataSource firebaseDataSource;
    private RecyclerView.OnScrollListener recyclerScrollListener;
    private MenuItem deleteMenuItem;
    private Chat chat;
    private TextView txtWriting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        getSupportActionBar().setTitle("");
        View view = getLayoutInflater().inflate(R.layout.action_bar_chat, null);
        LayoutParams layout = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        getSupportActionBar().setCustomView(view, layout);

        firebaseDataSource = FirebaseDataSource.getInstance();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        chatHelper = new ChatHelper(this);
        gallery = Gallery.init(this);
        gallery.setMaxSelection(1);
        gallery.setMediaTypeFilter(Gallery.IMAGE_TYPE_JPEG, Gallery.IMAGE_TYPE_PNG);
        selectedMessages = new ArrayList<>();

        chatHelper.setContactId(getIntent().getStringExtra("chatId"));
        getViewComponents();
        createListeners();

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycler.setLayoutManager(layoutManager);
        recycler.addOnItemTouchListener(new RecyclerItemClickListener(this, this));

        adapter = new ChatMessageAdapter(new ArrayList<Message>(), selectedMessages, this, chatHelper);

        List<Chat.Message> messages = firebaseDataSource.getChatMessages(chatHelper.getContactId());
        this.chat = firebaseDataSource.findChat(chatHelper.getContactId());

        if(this.chat == null){
            Contact contact = firebaseDataSource.findContact(chatHelper.getContactId());
            this.chat = new Chat();
            this.chat.setContactId(contact.getProfileId());
            this.chat.setProfile(contact);
        }

        ((OlaEscolaApplication) this.getApplication()).setChatSelected(this.chat);
        ImageView imgAvatar = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.imgAvatar);
        TextView txtName = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.txtName);
        Typeface font = FontManager.getTypeface(this, FontManager.WORK_SANS_BOLD);
        FontManager.markAsIconContainer(txtName, font);
        txtWriting = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.txtWriting);
        Typeface font1 = FontManager.getTypeface(this, FontManager.WORK_SANS);
        FontManager.markAsIconContainer(txtWriting, font1);

        try {
            if(chat.getProfile().getAvatar() != null) {
                Glide.with(this)
                .using(new FirebaseImageLoader())
                .load(FirebaseStorage
                        .getInstance().getReferenceFromUrl(getString(R.string.firebase_storage_avatars))
                        .child(chat.getProfile().getProfileId())
                        .child(chat.getProfile().getAvatar()))
                        .fallback(R.drawable.placeholder_user)
                .into(imgAvatar);
            }
            else{
                Glide.with(this)
                  .load(R.drawable.placeholder_user)
                  .asBitmap()
                  .diskCacheStrategy(DiskCacheStrategy.ALL)
                  .into(imgAvatar);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }

        txtName.setText(chat.getProfile().getDisplayName());
        
        if(messages != null) {
            adapter.setMessages(messages);
        }
        recycler.setAdapter(adapter);
    }
    
    private void createListeners() {

        iconSend.setText(R.string.ion_images);
        iconSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gallery.setRequestCode(REQUEST_FILE_CHOOSER).open();
            }
        });

        iconCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                if (ContextCompat.checkSelfPermission(ChatMessagesActivity.this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ChatMessagesActivity.this,
                            Manifest.permission.CAMERA)) {
                    } else {
                        ActivityCompat.requestPermissions(ChatMessagesActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                PERMISSION_CAMERA);

                    }
                }
                else{
                    startCamera();
                }
            } else{
                startCamera();
            }

            }
        });

        edtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtMessage.getText().toString().isEmpty()) {
                    iconSend.setText(R.string.ion_images);
                    iconSend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            gallery.setRequestCode(REQUEST_FILE_CHOOSER).open();
                        }
                    });
                } else {
                    iconSend.setText(R.string.ion_android_send);
                    iconSend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chatHelper.saveMessageWithHtml(edtMessage);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    scrollToBottom();
                                }
                            }, 1250);
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                FirebaseDatabase.getInstance()
                        .getReference()
                        .child("users").child(chat.getProfile().getProfileId())
                        .child("chats").child(FirebaseDataSource.getInstance().getUser().getUserId())
                        .child("writing").setValue(true);
            }
        });

        recyclerScrollListener = new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int firstPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                int lastPosition = layoutManager.findLastCompletelyVisibleItemPosition();

                if (firstPosition > -1 && lastPosition > -1) {
                    for (int i = firstPosition; i < lastPosition + 1; i++) {
                        Chat.Message message = adapter.getMessages().get(i);
                        FirebaseDataSource.getInstance().setChatMessageAsRead(message, chatHelper.getContactId());
                    }
                }
            }
        };

    }

    private void startCamera() {
        imageFile = ImageHelper.getUploadFilePath(UUID.randomUUID() + ".png");
        Uri outputImageUri = Uri.fromFile(imageFile);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputImageUri);
        startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startCamera();
                } else {
                    Toast.makeText(this, "Permissão negada para acesso a camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    public void scrollToBottom() {
        recycler.smoothScrollToPosition(adapter.getItemCount());
    }

        @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int lastPosition = adapter.getItemCount() > 0 ? adapter.getItemCount() - 1 : 0;
                int restoredPosition = preferences.getInt(RECYCLER_SCROLL_POSITION, lastPosition);
                recycler.scrollToPosition(restoredPosition);
            }
        }, 300);

        recycler.addOnScrollListener(recyclerScrollListener);
        FirebaseDataSource.getInstance().setOnChatsChangedListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        int firstVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition();
        if(firstVisiblePosition >= 0) {
            preferences.edit().putInt(RECYCLER_SCROLL_POSITION, firstVisiblePosition).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        imageName = UUID.randomUUID() + ".png";
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            hasImage = true;
        } else if (requestCode == REQUEST_FILE_CHOOSER && resultCode == RESULT_OK) {
            for (Uri uri : GalleryActivity.getSelection(intent)) {
                imageFile = new File(uri.getPath());
                hasImage = true;
            }
        }
    }

    private void getViewComponents() {
        findViewById(R.id.imageBackground).setAlpha(0.2f);
        recycler = (RecyclerView) findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        iconSend = (TextView) findViewById(R.id.iconSend);
        Typeface iconFont = FontManager.getTypeface(this, FontManager.IONICONS);
        FontManager.markAsIconContainer(iconSend, iconFont);
        iconSend.setText(getString(R.string.ion_images));
        edtMessage = (EditText) findViewById(R.id.edtMessage);
        edtMessage.clearFocus();
        iconCamera = (TextView) findViewById(R.id.iconCamera);
        FontManager.markAsIconContainer(iconCamera, iconFont);
        iconCamera.setText(getString(R.string.ion_android_camera));
    }

    public void sendImage(String subtitle) throws FileNotFoundException {
        final String imagePath = imageFile.getAbsolutePath();
        chatHelper.saveMessageWithImage(imageName, subtitle, imagePath);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollToBottom();
            }
        }, 1250);
        imageConfirmationFragment.dismiss();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (hasImage) {
            imageFile = ImageHelper.getUploadFilePath(imageFile, imageName);
            if (verifyImageSize()) return;
            bitmap = BitmapFactory.decodeFile(imageFile.getPath());
            imageConfirmationFragment = ImageConfirmationFragment.newInstance(bitmap);
            imageConfirmationFragment.show(getSupportFragmentManager(), "tag");
            hasImage = false;
        }
    }

    public boolean verifyImageSize() {
        long sizeInMB = imageFile.length() / 1024 / 1024;
        if( sizeInMB > 5){
            final Snackbar snackbar = Snackbar.make(this.recycler,
                    "A imagem tem " + sizeInMB +  "MB e o tamanho máximo permitido é 5MB",
                    BaseTransientBottomBar.LENGTH_INDEFINITE);
            snackbar.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
            hasImage = false;
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(View childView, int position) {

        if (selectedMessages.size() > 0) {
            toogleItemSelected(position);
            return;
        }

        Chat.Message message = adapter.getMessages().get(position);

        if(message.getType().equals("image")) {
            File source = null;
            if (message.getSenderId().equals(FirebaseDataSource.getInstance().getUser().getUserId())) {
                source = ImageHelper.getUploadFilePath(message.getContent());
            } else {
                source = ImageHelper.getDownloadFilePath("Chats", message.getContent());
            }

            ImageFullScreenFragment dialog = ImageFullScreenFragment.newInstance(source);
            dialog.show(this.getSupportFragmentManager(), "tag");
        }
    }

    @Override
    public void onItemLongPress(View childView, int position) {
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(50);
        toogleItemSelected(position);
    }

    @Override
    protected void onPause() {
        super.onPause();
        recycler.removeOnScrollListener(recyclerScrollListener);
        selectedMessages.clear();
        for(Chat.Message message: adapter.getMessages()){
            if(message.isDeleted()) message.setDeleted(false);
        }
        FirebaseDataSource.getInstance().setOnChatsChangedListener(null);
        ((OlaEscolaApplication) getApplication()).setChatSelected(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.chat_menu, menu);
        deleteMenuItem = menu.findItem(R.id.delete);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                removeMessages();
                break;
            default:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void removeMessages() {
        FirebaseDataSource.getInstance().removeChatMessages(selectedMessages, chatHelper.getContactId());
        deleteMenuItem.setVisible(false);
    }

    private void toogleItemSelected(int position) {

        Chat.Message message = adapter.getMessages().get(position);

        if (selectedMessages.contains(message)) {
            selectedMessages.remove(message);
            message.setDeleted(false);
            adapter.notifyDataSetChanged();
        } else {
            selectedMessages.add(message);
            message.setDeleted(true);
            adapter.notifyDataSetChanged();
        }

        if (selectedMessages.size() > 0) {
            deleteMenuItem.setVisible(true);
        } else {
            deleteMenuItem.setVisible(false);
        }
    }
    
    @Override
    public void onChanged(final Chat chat) {
        txtWriting = (TextView) this.findViewById(R.id.txtWriting);
        reloadChat(chat);
    }
    
    @Override
    public void onAdded(Chat chat) {
        reloadChat(chat);
    }
    
    @Override
    public void onRemoved(Chat chat) {
        reloadChat(chat);
    }
    
    public void reloadChat(Chat chat) {

        if(chat.getContactId().equals(this.chatHelper.getContactId()) && chat.isWriting()){
            txtWriting.setText("Está escrevendo...");
            txtWriting.setVisibility(View.VISIBLE);
        }
        else {
            Presence presence = FirebaseDataSource.getInstance().findPresence(chat.getContactId());
            if(presence != null && presence.isConnected()) {
                txtWriting.setText("online");
                txtWriting.setVisibility(View.VISIBLE);
            }
            else{
                txtWriting.setText("");
                txtWriting.setVisibility(View.GONE);
            }
        }


        if(adapter != null && chat != null && chat.getContactId().equals(chatHelper.getContactId())){
            List<Chat.Message> messages = firebaseDataSource.getChatMessages(chatHelper.getContactId());
            if(messages != null) {
                adapter.setMessages(messages);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                if (pastVisibleItems + visibleItemCount >= (totalItemCount - 1)) {
                    scrollToBottom();
                }
            }
        }
    }
}

