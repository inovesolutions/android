package br.com.olaescola.uirapuru.model;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by developer on 18/08/17.
 */

public class Message  implements Comparable<Message>, Cloneable{
   
   private String messageId;
   private Content content;
   private boolean deleted;
   private boolean received;
   private boolean read;
   
   public String getMessageId() {
      return messageId;
   }
   
   public void setMessageId(String messageId) {
      this.messageId = messageId;
   }
   
   public Content getContent() {
      return content;
   }
   
   public void setContent(Content content) {
      this.content = content;
   }
   
   public boolean isDeleted() {
      return deleted;
   }
   
   public void setDeleted(boolean deleted) {
      this.deleted = deleted;
   }
   
   public boolean isReceived() {
      return received;
   }
   
   public void setReceived(boolean received) {
      this.received = received;
   }
   
   public boolean isRead() {
      return read;
   }
   
   public void setRead(boolean read) {
      this.read = read;
   }
   
   public boolean equals(Message other) {
      return this.messageId.equals(other.messageId);
   }
   
   @Override
   public Object clone() throws CloneNotSupportedException {
      return super.clone();
   }
   
   @Override
   public int compareTo(@NonNull Message other) {
      return other.messageId.compareTo(this.messageId);
   }
   
   public static class Content{
      
      private Header header;
      private Body body;
      private String type;
   
      public Header getHeader() {
         return header;
      }
   
      public void setHeader(Header header) {
         this.header = header;
      }
   
      public Body getBody() {
         return body;
      }
   
      public void setBody(Body body) {
         this.body = body;
      }
   
      public String getType() {
         return type;
      }
   
      public void setType(String type) {
         this.type = type;
      }
   }
   
   public static class Header{
      
      private String date;
      private String moduleId;
      private Version moduleVersion;
      private String moduleIcon;
      private RGB moduleIconColor;
      private RGB moduleIconBackground;
      private String senderId;
      private String senderName;
      private String senderAvatar;
      private HashMap<String, Relation> relations;
      private List<Resource> resources;
   
      public String getDate() {
         return date;
      }
   
      public void setDate(String date) {
         this.date = date;
      }
   
      public String getModuleId() {
         return moduleId;
      }
   
      public void setModuleId(String moduleId) {
         this.moduleId = moduleId;
      }
   
      public Version getModuleVersion() {
         return moduleVersion;
      }
   
      public void setModuleVersion(Version moduleVersion) {
         this.moduleVersion = moduleVersion;
      }
   
      public String getModuleIcon() {
         return moduleIcon;
      }
   
      public void setModuleIcon(String moduleIcon) {
         this.moduleIcon = moduleIcon;
      }
   
      public RGB getModuleIconColor() {
         return moduleIconColor;
      }
   
      public void setModuleIconColor(RGB moduleIconColor) {
         this.moduleIconColor = moduleIconColor;
      }
   
      public RGB getModuleIconBackground() {
         return moduleIconBackground;
      }
   
      public void setModuleIconBackground(RGB moduleIconBackground) {
         this.moduleIconBackground = moduleIconBackground;
      }
   
      public String getSenderId() {
         return senderId;
      }
   
      public void setSenderId(String senderId) {
         this.senderId = senderId;
      }
   
      public String getSenderName() {
         return senderName;
      }
   
      public void setSenderName(String senderName) {
         this.senderName = senderName;
      }
   
      public String getSenderAvatar() {
         return senderAvatar;
      }
   
      public void setSenderAvatar(String senderAvatar) {
         this.senderAvatar = senderAvatar;
      }
   
      public HashMap<String, Relation> getRelations() {
         return relations;
      }
   
      public void setRelations(HashMap<String, Relation> relations) {
         this.relations = relations;
      }
   
      public List<Resource> getResources() {
         return resources;
      }
   
      public void setResources(List<Resource> resources) {
         this.resources = resources;
      }
   }
   
   public static class Body{
   
      private String title;
      private String subtitle;
      private String content;
      private String imageDescription;
   
      public String getTitle() {
         return title;
      }
   
      public void setTitle(String title) {
         this.title = title;
      }
   
      public String getSubtitle() {
         return subtitle;
      }
   
      public void setSubtitle(String subtitle) {
         this.subtitle = subtitle;
      }
   
      public String getContent() {
         return content;
      }
   
      public void setContent(String content) {
         this.content = content;
      }

      public String getImageDescription() {
         return imageDescription;
      }

      public void setImageDescription(String imageDescription) {
         this.imageDescription = imageDescription;
      }
   }
}
