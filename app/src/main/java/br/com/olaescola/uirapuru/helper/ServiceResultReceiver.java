package br.com.olaescola.uirapuru.helper;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

/**
 * Created by developer on 21/09/17.
 */

public class ServiceResultReceiver extends ResultReceiver {

   private Receiver receiver;
   
   public ServiceResultReceiver(Handler handler) {
      super(handler);
   }
   
   public interface Receiver{
      public void onReceiveResult(int resultCode, Bundle resultData);
   }
   
   public void setReceiver(Receiver receiver) {
      this.receiver = receiver;
   }
   
   @Override
   protected void onReceiveResult(int resultCode, Bundle resultData) {
      if(receiver != null){
         receiver.onReceiveResult(resultCode, resultData);
      }
   }
}
