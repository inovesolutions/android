package br.com.olaescola.uirapuru.model;

import java.util.List;

/**
 * Created by developer on 24/08/17.
 */

public class Relation{
   private List<String> types;
   private Profile profile;
   
   public List<String> getTypes() {
      return types;
   }
   
   public void setTypes(List<String> types) {
      this.types = types;
   }

   public Profile getProfile() {
      return profile;
   }

   public void setProfile(Profile profile) {
      this.profile = profile;
   }
}
