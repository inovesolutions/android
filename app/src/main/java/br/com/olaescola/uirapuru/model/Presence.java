package br.com.olaescola.uirapuru.model;

/**
 * Created by developer on 05/10/17.
 */

public class Presence {
   private String name;
   private boolean connected;
   private Long timestamp;
   
   public String getName() {
      return name;
   }
   
   public void setName(String name) {
      this.name = name;
   }
   
   public boolean isConnected() {
      return connected;
   }
   
   public void setConnected(boolean connected) {
      this.connected = connected;
   }
   
   public Long getTimestamp() {
      return timestamp;
   }
   
   public void setTimestamp(Long timestamp) {
      this.timestamp = timestamp;
   }
}
