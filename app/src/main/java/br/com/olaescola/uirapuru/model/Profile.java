package br.com.olaescola.uirapuru.model;

/**
 * Created by developer on 18/08/17.
 */

public class Profile {
   
   private String profileId;
   private String avatar;
   private String displayName;
   private String email;
   private String gender;
   private String mobilePhone;
   private String type;
   
   public String getProfileId() {
      return profileId;
   }
   
   public void setProfileId(String profileId) {
      this.profileId = profileId;
   }
   
   public String getAvatar() {
      return avatar;
   }
   
   public void setAvatar(String avatar) {
      this.avatar = avatar;
   }
   
   public String getDisplayName() {
      return displayName;
   }
   
   public void setDisplayName(String displayName) {
      this.displayName = displayName;
   }
   
   public String getEmail() {
      return email;
   }
   
   public void setEmail(String email) {
      this.email = email;
   }
   
   public String getGender() {
      return gender;
   }
   
   public void setGender(String gender) {
      this.gender = gender;
   }
   
   public String getMobilePhone() {
      if(mobilePhone == null) mobilePhone = "";
      return mobilePhone;
   }
   
   public void setMobilePhone(String mobilePhone) {
      this.mobilePhone = mobilePhone;
   }
   
   public String getType() {
      return type;
   }
   
   public void setType(String type) {
      this.type = type;
   }
}
