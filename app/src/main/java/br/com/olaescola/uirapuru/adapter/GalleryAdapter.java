package br.com.olaescola.uirapuru.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.model.Image;

/**
 * Created by rafae on 22/06/2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ItemRowHolder> {

    private Context context;
    private final ArrayList<Image> images;

    private ImageSelectedListener imageSelectedListener;

    public GalleryAdapter(Context ctx, ArrayList<Image> images) {
        this.images = images;
        this.context = ctx;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gallery, null);
        return new ItemRowHolder(v);
    }

    @Override
    public void onBindViewHolder(final ItemRowHolder view, final int i) {

        Glide.with(context)
                .load(new File(images.get(i).getImgPath().getAbsolutePath())).into(view.imageView);

        view.checkbox.setChecked(images.get(i).isSelected());
        view.checkbox.setTag(images.get(i));

        view.checkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                selectImage(view, i);
            }
        });

        view.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(view.checkbox.isChecked()) view.checkbox.setChecked(false);
                else view.checkbox.setChecked(true);
                selectImage(view, i);
            }
        });
    }

    private void selectImage(ItemRowHolder view, int i) {
        Image image = (Image) view.checkbox.getTag();
        image.setSelected(view.checkbox.isChecked());
        images.get(i).setSelected(view.checkbox.isChecked());
        if(imageSelectedListener != null) imageSelectedListener.onImageSelected(image);
    }

    public ArrayList<Image> getSelected() {
        ArrayList<Image> images = new ArrayList<>();
        for (int i = 0; i < this.images.size(); i++) {
            if (this.images.get(i).isSelected()) {
                images.add(this.images.get(i));
            }

        }
        return images;
    }


    protected class ItemRowHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private CheckBox checkbox;

        private ItemRowHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.imgThumb);
            this.checkbox = (CheckBox) view.findViewById(R.id.checkBox);
        }
    }

    @Override
    public int getItemCount() {
        return (null != images ? images.size() : 0);
    }

    public interface ImageSelectedListener{
        void onImageSelected(Image image);
    }

    public void setImageSelectedListener(ImageSelectedListener imageSelectedListener) {
        this.imageSelectedListener = imageSelectedListener;
    }

}
