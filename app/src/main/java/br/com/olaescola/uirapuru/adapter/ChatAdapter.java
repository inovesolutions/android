package br.com.olaescola.uirapuru.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.easyandroidanimations.library.ScaleInAnimation;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;
import br.com.olaescola.uirapuru.helper.DateHelper;
import br.com.olaescola.uirapuru.helper.FontManager;
import br.com.olaescola.uirapuru.helper.HtmlHelper;
import br.com.olaescola.uirapuru.helper.ImageHelper;
import br.com.olaescola.uirapuru.model.Chat;

/**
 * Created by rafae on 06/06/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter {

    private final List<Chat> selectedChats;
    private final LinearLayoutManager layout;
    private List<Chat> chats;
    private Context context;

    public ChatAdapter(List<Chat> chats, List<Chat> selectedChats, Context context, LinearLayoutManager layout) {
        this.chats = chats;
        this.selectedChats = selectedChats;
        this.context = context;
        this.layout = layout;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.chat_item, parent, false);
        ViewHolder holder = new ViewHolder(view, this.context);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final ViewHolder viewHolder = (ViewHolder) holder;
        final Chat chat = this.chats.get(position);
        viewHolder.chat = chat;
    
        viewHolder.toName.setText(chat.getProfile().getDisplayName());
        
        if (chat.getBadge() > 0) {
            viewHolder.badge.setVisibility(View.VISIBLE);
            viewHolder.badge.setText(String.valueOf(chat.getBadge()));
        } else {
            viewHolder.badge.setVisibility(View.GONE);
        }
    
        try {
            if (selectedChats.contains(chat)) {
                viewHolder.itemView
                      .setBackground(ContextCompat.getDrawable(context, R.drawable.background_message_selected));
            } else {
                viewHolder.itemView
                      .setBackground(ContextCompat.getDrawable(context, R.drawable.white));
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        List<Chat.Message> messages = FirebaseDataSource.getInstance().getChatMessages(chat.getContactId());

        Iterator<Chat.Message> iterator = messages.iterator();

        while (iterator.hasNext()){
            Chat.Message message = iterator.next();
            if(message.isDeleted()) iterator.remove();
        }

        if(messages != null && messages.size() > 0) {
            viewHolder.addLastMessage(messages.get(messages.size() - 1));
        }

        if (chat.getProfile().getAvatar() != null) {

            final File avatar = ImageHelper.getDownloadFilePath("Avatars", chat.getProfile().getAvatar());
            if (avatar.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(avatar.getPath());
                if (bitmap != null) {
                    viewHolder.avatar.setImageBitmap(bitmap);
                }
            } else {
                try {
                    FirebaseStorage
                    .getInstance().getReferenceFromUrl(context
                    .getString(R.string.firebase_storage_avatars)
                    .concat("/").concat(chat.getProfile().getProfileId())
                    .concat("/").concat(chat.getProfile().getAvatar()))
                    .getFile(avatar)
                    .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            Bitmap bitmap = BitmapFactory.decodeFile(avatar.getPath());
                            viewHolder.avatar.setImageBitmap(bitmap);
                        }
                    });
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                            R.drawable.placeholder_user);
                    viewHolder.avatar.setImageBitmap(bitmap);
                }
            }
        }
        else {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.placeholder_user);
            viewHolder.avatar.setImageBitmap(bitmap);
        }

        viewHolder.itemView.setVisibility(View.VISIBLE);

    }
    

    public List<Chat> getChats() {
        return chats;
    }

    public void setChats(List<Chat> newChats) {
        this.chats.clear();
        this.chats.addAll(newChats);
        
        Collections.sort(this.chats, new Comparator<Chat>() {
            @Override
            public int compare(Chat o1, Chat o2) {
                return o1.compareTo(o2);
            }
        });

        this.notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return (chats == null? 0 : chats.size());
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        
        private Date lastMessageDate;
        private TextView toName;
        private TextView lastUpdate;
        private TextView lastMessage;
        private ImageView avatar;
        private TextView badge;
        private Context context;
        public Chat chat;

        public ViewHolder(View itemView, Context context) {
            
            super(itemView);
            this.context = context;
            this.toName = (TextView) itemView.findViewById(R.id.txtName);
            Typeface font = FontManager.getTypeface(context, FontManager.WORK_SANS_BOLD);
            FontManager.markAsIconContainer(toName, font);
            this.lastMessage = (TextView) itemView.findViewById(R.id.txtLastMessage);
            this.lastUpdate = (TextView) itemView.findViewById(R.id.txtLastUpdate);
            this.avatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
            this.badge = (TextView) itemView.findViewById(R.id.txtBadge);

            this.itemView.setVisibility(View.GONE);
            this.toName.setText("");
            this.lastMessage.setText("");
            this.lastUpdate.setText("");
            this.badge.setVisibility(View.GONE);
        }

        private void addLastMessage(Chat.Message message) {

            if(message.getDate() != null) {
                Date date = new Date(message.getDate());

                if (lastMessageDate instanceof Date) {
                    if (date.getTime() >= lastMessageDate.getTime()) {
                        lastMessageDate = date;
                    }
                } else {
                    lastMessageDate = date;
                }

                try {

                    lastUpdate.setText(DateHelper.formatToYesterdayOrToday(new Date(message.getDate())));

                    switch (message.getType()) {
                        case "text":
                            lastMessage.setText(HtmlHelper.fromHtml(message.getContent()));
                            break;
                        case "image":
                            lastMessage
                                    .setText(HtmlHelper.fromHtml("<b>" + context.getString(R.string.image) + "</b>"));
                            break;
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
