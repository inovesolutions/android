package br.com.olaescola.uirapuru;

import android.app.Application;
import android.os.StrictMode;

import com.google.firebase.database.FirebaseDatabase;

import br.com.olaescola.uirapuru.model.Chat;

public class OlaEscolaApplication extends Application{

   private Chat chatSelected;
   @Override
   public void onCreate() {
      super.onCreate();
      StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
      StrictMode.setVmPolicy(builder.build());
      FirebaseDatabase.getInstance().setPersistenceEnabled(true);
   }

   public Chat getChatSelected() {
      return chatSelected;
   }

   public void setChatSelected(Chat chatSelected) {
      this.chatSelected = chatSelected;
   }
}
