package br.com.olaescola.uirapuru.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by rafae on 14/06/2017.
 */

public class IncomingSmsReceiver extends BroadcastReceiver {

    final SmsManager sms = SmsManager.getDefault();
    private static OnSmsReceivedListener listener = null;
    @Override
    public void onReceive(Context context, Intent intent) {
        if(FirebaseAuth.getInstance().getCurrentUser() != null) return;
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    if (listener != null) {
                        listener.onSmsReceived(senderNum, message);
                    }

                }
            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }

    public static void setOnSmsReceivedListener(Context context) {
        listener = (OnSmsReceivedListener) context;
    }

    public interface OnSmsReceivedListener{
        public void onSmsReceived(String sender, String message);
    }
}
