package br.com.olaescola.uirapuru.model;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.olaescola.uirapuru.firebase.FirebaseDataSource;

/**
 * Created by developer on 18/08/17.
 */

public class Chat implements Comparable<Chat>{
   
   private String contactId;
   private Profile profile;
   private Long lastChange;
   private boolean empty;
   private List<Message> messages;
   private boolean writing;
   
   public String getContactId() {
      return contactId;
   }
   
   public void setContactId(String contactId) {
      this.contactId = contactId;
   }
   
   public Profile getProfile() {
      return profile;
   }
   
   public void setProfile(Profile profile) {
      this.profile = profile;
   }
   
   public Long getLastChange() {
      if(lastChange == null) lastChange = Long.valueOf(0);
      return lastChange;
   }
   
   public void setLastChange(Long lastChange) {
      this.lastChange = lastChange;
   }
   
   public boolean isEmpty() {
      return empty;
   }
   
   public void setEmpty(boolean empty) {
      this.empty = empty;
   }

   public boolean isWriting() {
      return writing;
   }

   public void setWriting(boolean writing) {
      this.writing = writing;
   }

   public List<Message> getMessages() {
      if(messages == null) messages = new ArrayList<>();
      return messages;
   }
   
   public void setMessages(List<Message> messages) {
      this.messages = messages;
   }
   
   public int getBadge() {
      int badge = 0;
      if(messages != null){
         for(Message message: messages){
            if(!message.getSenderId().equals(FirebaseDataSource.getInstance().getUser().getUserId())
                  && !message.isRead()) badge++;
         }
      }
      return badge;
   }
   
   public Chat fill(DataSnapshot dataSnapshot) {
      this.contactId = dataSnapshot.getKey();
      JSONObject chatData = new JSONObject((HashMap) dataSnapshot.getValue());
      try {
         
         if(chatData.has("lastChange")) {
            this.lastChange = chatData.getLong("lastChange");
         }

         if(chatData.has("writing")) {
            this.writing = chatData.getBoolean("writing");
         }
         
         this.profile = dataSnapshot.child("profile").getValue(Profile.class);
         this.profile.setProfileId(this.contactId);
         DataSnapshot messagesSnapshot = dataSnapshot.child("messages");
         this.getMessages().clear();
         
         if(messagesSnapshot.exists()){
            
            Iterator<DataSnapshot> messagesIterator = messagesSnapshot.getChildren().iterator();
            
            while(messagesIterator.hasNext()){
               DataSnapshot nextMessageSnapshot = messagesIterator.next();
               Chat.Message message = nextMessageSnapshot.getValue(Chat.Message.class);
               message.setMessageId(nextMessageSnapshot.getKey());
               if(!message.isDeleted()) {
                  this.getMessages().add(message);
                  if(!FirebaseDataSource.getInstance().currentUserIsSender(message) && !message.isReceived()) {

                     message.setReceived(true);

                     FirebaseDatabase.getInstance().getReference()
                             .child("users")
                             .child(FirebaseDataSource.getInstance().getUser().getUserId())
                             .child("chats").child(this.getContactId())
                             .child("messages").child(message.getMessageId())
                             .child("received").setValue(true);

                     FirebaseDatabase.getInstance().getReference()
                             .child("users")
                             .child(this.getContactId())
                             .child("chats").child(FirebaseDataSource.getInstance().getUser().getUserId())
                             .child("messages").child(message.getMessageId())
                             .child("received").setValue(true);
                  }
               }
            }
            
         }
      } catch (JSONException e) {
         e.printStackTrace();
      }
      return this;
   }
   
   @Override
   public int compareTo(@NonNull Chat other) {
      return other.contactId.compareTo(this.contactId);
   }

   public int compareTo(Long lastChange){
      return lastChange.compareTo(this.lastChange);
   }
   
   public static class Message {
      
      private String contactId;
      private String messageId;
      private String content;
      private Long date;
      private boolean deleted;
      private boolean sent;
      private boolean received;
      private boolean read;
      private String senderId;
      private String type;
      private String subtitle;
      private String path;
      private boolean uploaded;
      private Version version;
      
      public String getContactId() {
         return contactId;
      }
      
      public void setContactId(String contactId) {
         this.contactId = contactId;
      }
      
      public String getMessageId() {
         return messageId;
      }
      
      public void setMessageId(String messageId) {
         this.messageId = messageId;
      }
      
      public String getContent() {
         return content;
      }
      
      public void setContent(String content) {
         this.content = content;
      }
      
      public Long getDate() {
         return date;
      }
      
      public void setDate(Long date) {
         this.date = date;
      }
   
      public boolean isDeleted() {
         return deleted;
      }
   
      public void setDeleted(boolean deleted) {
         this.deleted = deleted;
      }
   
      public boolean isSent() {
         return sent;
      }
   
      public void setSent(boolean sent) {
         this.sent = sent;
      }
   
      public boolean isReceived() {
         return received;
      }
   
      public void setReceived(boolean received) {
         this.received = received;
      }
   
      public boolean isRead() {
         return read;
      }
   
      public void setRead(boolean read) {
         this.read = read;
      }
   
      public String getSenderId() {
         return senderId;
      }
      
      public void setSenderId(String senderId) {
         this.senderId = senderId;
      }
   
      public String getType() {
         return type;
      }
   
      public void setType(String type) {
         this.type = type;
      }
   
      public String getSubtitle() {
         return subtitle;
      }
   
      public void setSubtitle(String subtitle) {
         this.subtitle = subtitle;
      }
   
      public String getPath() {
         return path;
      }
   
      public void setPath(String path) {
         this.path = path;
      }

      public boolean isUploaded() {
         return uploaded;
      }

      public void setUploaded(boolean uploaded) {
         this.uploaded = uploaded;
      }

      public Version getVersion() {
         return version;
      }
   
      public void setVersion(Version version) {
         this.version = version;
      }

      public boolean equals(Message other) {
         return other.messageId.equals(this.messageId);
      }
      
      public Map toMap(){
         Map map = new HashMap();
         map.put("content", this.content);
         map.put("date", ServerValue.TIMESTAMP);
         map.put("path", this.path);
         map.put("sent", this.sent);
         map.put("received", this.received);
         map.put("read", this.read);
         map.put("deleted", this.deleted);
         map.put("version", this.version);
         map.put("uploaded", this.uploaded);
         map.put("subtitle", this.subtitle);
         map.put("type", this.type);
         map.put("senderId", this.senderId);
         
         return map;
      }
   }
   
}
