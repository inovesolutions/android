package br.com.olaescola.uirapuru.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import br.com.olaescola.uirapuru.MainActivity;
import br.com.olaescola.uirapuru.OlaEscolaApplication;
import br.com.olaescola.uirapuru.R;
import br.com.olaescola.uirapuru.model.Chat;

public class OlaEscolaFirebaseMessagingService extends FirebaseMessagingService {

    private RemoteMessage remoteMessage;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        this.remoteMessage = remoteMessage;
        Chat chat = ((OlaEscolaApplication) getApplication()).getChatSelected();
        if(chat == null || !this.remoteMessage.getData().get("senderId").equals(chat.getContactId())) {
            new CustomTask().execute((Void[]) null);
        }

        super.onMessageReceived(remoteMessage);

    }

    private class CustomTask extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... param) {
            return null;
        }

        protected void onPostExecute(Void param) {

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(OlaEscolaFirebaseMessagingService.this)
                            .setSmallIcon(R.drawable.icon);

            if(remoteMessage.getData().get("collection").equals("chats")) {
                mBuilder.setContentTitle(remoteMessage.getData().get("senderName"));
                if (remoteMessage.getData().get("type").equals("text")) {
                    mBuilder.setContentText(remoteMessage.getData().get("content"));
                } else {
                    mBuilder.setContentText("Enviou uma imagem");
                }
            }
            else if(remoteMessage.getData().get("collection").equals("messages")){
                mBuilder.setContentTitle(remoteMessage.getData().get("title"));
                mBuilder.setContentText(remoteMessage.getData().get("subtitle"));
            }

            Intent resultIntent = new Intent(OlaEscolaFirebaseMessagingService.this, MainActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder
                    .create(OlaEscolaFirebaseMessagingService.this);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT );
            mBuilder.setContentIntent(resultPendingIntent);

            mBuilder.setAutoCancel(true);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(0, mBuilder.build());
        }
    }
}
