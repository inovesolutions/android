package br.com.olaescola.uirapuru.model;

import java.io.File;

/**
 * Created by developer on 25/08/17.
 */

public class Image {
   private File imgPath;
   private boolean selected;
   
   public File getImgPath() {
      return imgPath;
   }
   
   public void setImgPath(File imgPath) {
      this.imgPath = imgPath;
   }
   
   public boolean isSelected() {
      return selected;
   }
   
   public void setSelected(boolean selected) {
      this.selected = selected;
   }
}
