package br.com.olaescola.uirapuru.model;

/**
 * Created by developer on 05/09/17.
 */

public class Resource{
   
   private boolean downloaded;
   private Long downloadDate;
   private String name;
   
   public boolean isDownloaded() {
      return downloaded;
   }
   
   public void setDownloaded(boolean downloaded) {
      this.downloaded = downloaded;
   }
   
   public Long getDownloadDate() {
      return downloadDate;
   }
   
   public void setDownloadDate(Long downloadDate) {
      this.downloadDate = downloadDate;
   }
   
   public String getName() {
      return name;
   }
   
   public void setName(String name) {
      this.name = name;
   }
}
